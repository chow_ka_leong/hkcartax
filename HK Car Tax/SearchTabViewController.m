//
//  SearchTabViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "SearchTabViewController.h"
#import "HistoryTabViewController.h"
#import "SearchManager.h"

@interface SearchTabViewController () <UIAlertViewDelegate>

@end

@implementation SearchTabViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        Language *lang = [Language sharedInstance];
        self.title = [lang get:@"menu_btn_search_buyer"];
        self.navigationItem.rightBarButtonItem.title = [lang get:@"history"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"green_gradient.png"]];
    
    self.topTabView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.topTabView.layer.shadowOffset = CGSizeMake(0.0f, 0.5f);
    self.topTabView.layer.shadowOpacity = 0.5f;
    
    Language *lang = [Language sharedInstance];
    
    [self.buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIButton *btn = (UIButton *)obj;
        NSString *btnStr = [NSString stringWithFormat:@"searchTab%lu", idx + 1];
        [btn setTitle:[lang get:btnStr] forState:UIControlStateNormal];
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    }];
    
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    UIButton *btn = [self.buttons objectAtIndex:self.selectedIndex];
    NSString *identifierStr = [NSString stringWithFormat:@"viewController%lu", self.selectedIndex + 1];
    [self performSegueWithIdentifier:identifierStr sender:btn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self orientationMask];
}

- (UIInterfaceOrientationMask)orientationMask
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([[userDefault valueForKey:shallAllowLandscape]boolValue] == YES) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    if ([segue.identifier rangeOfString:@"viewController"].location != NSNotFound) {
        NSString *indexStr = [segue.identifier stringByReplacingOccurrencesOfString:@"viewController" withString:@""];
        self.selectedIndex = [indexStr intValue] - 1;
    
    } else if ([segue.identifier isEqualToString:@"historySegue"]) {
        HistoryTabViewController *historyCtrl = (HistoryTabViewController *)segue.destinationViewController;
        historyCtrl.parentViewCtrl = self;
        
        if ([self.destinationIdentifier isEqualToString:@"viewController1"]) {
            historyCtrl.selectedIndex = 0;
        } else if ([self.destinationIdentifier isEqualToString:@"viewController2"]) {
            historyCtrl.selectedIndex = 1;
        } else if ([self.destinationIdentifier isEqualToString:@"viewController3"]) {
            historyCtrl.selectedIndex = 2;
        }
    }
}

- (void)refresh
{
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
    Language *lang = [Language sharedInstance];
    
    SearchManager *searchMgr = [SearchManager sharedManager];

    [searchMgr getMakeList:^(BOOL success, NSString *errStr) {
        if (success) {
            [searchMgr getModelList:^(BOOL success, NSString *errStr2) {
                [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
                if (!success) {
                    if ([errStr2 isEqualToString:[lang get:@"errorServerNotAvailable"]]) {
                        [Utils showOnMaintenance:self serverDown:YES];
                    } else if ([errStr2 isEqualToString:[lang get:@"errorServerMaintenance"]]) {
                        [Utils showOnMaintenance:self serverDown:NO];
                    } else {
                        [Utils showAlertWithTitle:errStr2 message:nil];
                    }
                }
            }];
        } else {
            [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
            
            if ([errStr isEqualToString:[lang get:@"errorServerNotAvailable"]]) {
                [Utils showOnMaintenance:self serverDown:YES];
            } else if ([errStr isEqualToString:[lang get:@"errorServerMaintenance"]]) {
                [Utils showOnMaintenance:self serverDown:NO];
            } else {
                [Utils showAlertWithTitle:errStr message:nil];
            }
        }
    }];
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
}
@end
