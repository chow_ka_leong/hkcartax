//
//  HistoryTabViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 27/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "MHCustomTabBarController.h"

@class SearchTabViewController;

@interface HistoryTabViewController : MHCustomTabBarController

@property (weak, nonatomic) IBOutlet UIView *topTabView;
@property (nonatomic) NSUInteger selectedIndex;
@property (weak, nonatomic) SearchTabViewController *parentViewCtrl;

@end
