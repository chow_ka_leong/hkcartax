//
//  SearchVehicleViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPAutoCompleteTextField.h"

@interface SearchVehicleViewController : UIViewController <UITextFieldDelegate, MLPAutoCompleteTextFieldDelegate, MLPAutoCompleteTextFieldDataSource, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *makeTextField;
@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *modelTextField;
@property (weak, nonatomic) IBOutlet UITextField *manuYearTextField;
@property (weak, nonatomic) IBOutlet UITextField *modelYearTextField;
@property (weak, nonatomic) IBOutlet UITextField *modelCodeTextField;
@property (weak, nonatomic) IBOutlet UILabel *remarkLbl;
@property (weak, nonatomic) IBOutlet UILabel *remarkBlueLbl;
@property (weak, nonatomic) IBOutlet UILabel *mandatoryLbl;
@property (weak, nonatomic) IBOutlet UILabel *noSymbolLbl;

- (IBAction)searchPressed;
- (IBAction)resetPressed;

@end
