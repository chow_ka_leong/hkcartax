//
//  AutoItemTableCell.m
//  HK Car Tax
//
//  Created by Rocky Wong on 9/5/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "AutoItemTableCell.h"

@implementation AutoItemTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.detailLbl setCenterY:self.contentView.center.y];
    [self.titleLbl setY:self.detailLbl.y];
}

@end
