//
//  SearchHistoryViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 24/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchHistoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *historyList;

@end

@interface HistoryVehicleViewController : SearchHistoryViewController <UIAlertViewDelegate>

@end

@interface HistoryChassisViewController : SearchHistoryViewController <UIAlertViewDelegate>

@end

@interface HistoryPRPViewController : SearchHistoryViewController <UIAlertViewDelegate>

@end
