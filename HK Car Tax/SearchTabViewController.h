//
//  SearchTabViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "MHCustomTabBarController.h"

@interface SearchTabViewController : MHCustomTabBarController

@property (weak, nonatomic) IBOutlet UIView *topTabView;
@property (nonatomic) NSUInteger selectedIndex;
@end
