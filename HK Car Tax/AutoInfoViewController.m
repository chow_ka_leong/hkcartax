//
//  AutoInfoViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "AutoInfoViewController.h"
#import "AutoItemTableCell.h"
#import "APIManager.h"
#import "FRTWebViewController.h"

@interface AutoInfoViewController () <UIAlertViewDelegate>

@end

@implementation AutoInfoViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    Language *lang = [Language sharedInstance];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 0)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.backgroundColor = [UIColor clearColor];
    [lbl setMultipleLineTextAndResize:[lang get:@"remarkSearchResult"]];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, lbl.frame.size.height + 10 + 44)];
    [footerView addSubview:lbl];
    
    self.tableView.tableFooterView = footerView;
    
    
    [self.calculatorButton setTitle:[lang get:@"calculatorBtnText"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions

-(IBAction)calculatorButtonDidClick:(id)sender {
    
    Language *lang = [Language sharedInstance];
    NSString *alertMsg = [lang get:@"alertGoToCalculatorMessage"];
    NSString *backBtnText = [lang get:@"back"];
    NSString *confirmBtnText = [lang get:@"confirm"];
    
    if (NSStringFromClass([UIAlertController class])) {
        UIAlertController *alertCtrl = [UIAlertController alertControllerWithTitle:nil message:alertMsg preferredStyle:UIAlertControllerStyleAlert];
        
        [alertCtrl addAction:[UIAlertAction actionWithTitle:backBtnText style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }]];
        
        [alertCtrl addAction:[UIAlertAction actionWithTitle:confirmBtnText style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self goToCalculator];
        }]];
        
        [self presentViewController:alertCtrl animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:alertMsg delegate:self cancelButtonTitle:backBtnText otherButtonTitles:confirmBtnText, nil];
        
        [alertView show];
    }
    
}

-(void) goToCalculator {
    
    RoleManager *roleMgr = [RoleManager sharedManager];
    NSString *roleKeyStr = [roleMgr getResourceStr:@"menu_btn_cal"];
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    APIManager *api = [APIManager sharedManager];
    
    Language *lang = [Language sharedInstance];
    
    NSString * url_Str = @"";
    
    if([lang en]){
        url_Str = CALCULATOR_EN_URL;
    }
    else if([lang tc]){
        url_Str = CALCULATOR_TC_URL;
    }
    else{
        url_Str = CALCULATOR_SC_URL;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@?price_a1=%@", url_Str, self.autoDetail.a1PriceStr];

    [api getAuthRequest:url callback:^(NSMutableURLRequest *req) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if (req) {
            FRTWebViewController *webViewCtrl = [[FRTWebViewController alloc] init];
            webViewCtrl.request = req;
            webViewCtrl.title = [[Language sharedInstance] get:roleKeyStr];
            [self.navigationController pushViewController:webViewCtrl animated:YES];
        }
    }];
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        [self goToCalculator];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.autoDetail.sectionList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AutoSection *autoSection = [self.autoDetail.sectionList objectAtIndex:section];
    return [autoSection.afList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *BasicCellIdentifier = @"BasicCellIdentifier";
    static NSString *PriceCellIdentifier = @"PriceCellIdentifier";
    static NSString *ItemCellIdentifier = @"ItemCellIdentifier";
    
    UITableViewCell *cell;
    AutoField *af = [self.autoDetail getFieldAtIndexPath:indexPath];
    Language *lang = [Language sharedInstance];
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:BasicCellIdentifier forIndexPath:indexPath];
        cell.textLabel.text = [lang get:af.name];
        cell.detailTextLabel.text = af.value;
        
    } else if (indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:PriceCellIdentifier forIndexPath:indexPath];
        cell.textLabel.text = [lang get:af.name];
        cell.detailTextLabel.text = af.value;
        
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:ItemCellIdentifier forIndexPath:indexPath];
        AutoItemTableCell *itemCell = (AutoItemTableCell *)cell;
        itemCell.titleLbl.text = [lang get:af.name];
        [itemCell.detailLbl setMultipleLineTextAndResize:af.value];
        [itemCell.titleLbl sizeToFit];
    }

    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UIFont *titleFont = [UIFont systemFontOfSize:15];
//    UIFont *detailFont = [UIFont systemFontOfSize:13];
//    CGSize constraint = CGSizeMake(250, CGFLOAT_MAX);
//    
//    AutoField *af = [self.autoDetail getFieldAtIndexPath:indexPath];
//    
//    Language *lang = [Language sharedInstance];
//    NSString *titleStr = [lang get:af.name];
//    NSString *detailStr = af.value;
//    
//    CGSize titleSize = [titleStr sizeWithFont:titleFont constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
//    CGSize detailSize = [detailStr sizeWithFont:detailFont constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
//    int height = titleSize.height + detailSize.height;
//    if (height <= 45)
//        return 45;
//    else
//        return height + 10;
    
    if (indexPath.section == 0 || indexPath.row == 0)
        return 44;
    
    UIFont *detailFont = [UIFont systemFontOfSize:14];
    CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width - 62, CGFLOAT_MAX);
    
    AutoField *af = [self.autoDetail getFieldAtIndexPath:indexPath];
    CGSize detailSize = [af.value sizeWithFont:detailFont constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    int height = detailSize.height + 22;
    if (height < 44)
        return 44;
    else
        return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    float xOffset = 10;
    float yOffset = 5;
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 0, 320 - 2 * xOffset, 0)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = THEME_GREEN;
    lbl.backgroundColor = [UIColor clearColor];
    
    AutoSection *autoSection = [self.autoDetail.sectionList objectAtIndex:section];
    Language *lang = [Language sharedInstance];
    [lbl setMultipleLineTextAndResize:[lang get:autoSection.name]];

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, lbl.frame.size.height + yOffset)];
    [view addSubview:lbl];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    float xOffset = 10;
    float yOffset = 5;
    
    AutoSection *autoSection = [self.autoDetail.sectionList objectAtIndex:section];
    if (autoSection.name) {
        Language *lang = [Language sharedInstance];
        NSString *text = [lang get:autoSection.name];
        UIFont *font = [UIFont systemFontOfSize:13];
        CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(320 - 2 * xOffset, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
        return size.height + yOffset;
        
    } else {
        return 10;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(tintColor)]) {
        CGFloat cornerRadius = 5.f;
        cell.backgroundColor = UIColor.clearColor;
        CAShapeLayer *layer = [[CAShapeLayer alloc] init];
        CGMutablePathRef pathRef = CGPathCreateMutable();
        CGRect bounds = CGRectInset(cell.bounds, 5, 0);
        BOOL addLine = NO;
        if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
            CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
        } else if (indexPath.row == 0) {
            CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
            CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
            addLine = YES;
        } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
            CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
            CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
        } else {
            CGPathAddRect(pathRef, nil, bounds);
            addLine = YES;
        }
        layer.path = pathRef;
        CFRelease(pathRef);
        layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
        
        if (addLine == YES) {
            CALayer *lineLayer = [[CALayer alloc] init];
            CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
            lineLayer.frame = CGRectMake(CGRectGetMinX(bounds)+5, bounds.size.height-lineHeight, bounds.size.width-5, lineHeight);
            lineLayer.backgroundColor = tableView.separatorColor.CGColor;
            [layer addSublayer:lineLayer];
        }
        UIView *testView = [[UIView alloc] initWithFrame:bounds];
        [testView.layer insertSublayer:layer atIndex:0];
        testView.backgroundColor = UIColor.clearColor;
        cell.backgroundView = testView;
    }
}


@end
