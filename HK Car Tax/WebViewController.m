//
//  WebViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 24/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController () <UIAlertViewDelegate>

@end

@implementation WebViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    webView.delegate = self;
    [self.view addSubview:webView];
    self.webView = webView;
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [self.webView loadRequest:self.request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
//    NSCachedURLResponse *resp = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
//    NSDictionary *headers = [(NSHTTPURLResponse*)resp.response allHeaderFields];
//
//    NSString *isMaintenance = headers[@"Is-SCG-Maintenance"];
//
//    if (isMaintenance) {
//        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
//        [Utils showOnMaintenance:self serverDown:NO];
//    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
    if (![Utils isServerReachable]) {
        [Utils showNotReachable:self];
    } else {
        
        NSCachedURLResponse *resp = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
        NSDictionary *headers = [(NSHTTPURLResponse*)resp.response allHeaderFields];
        
        NSString *isMaintenance = headers[@"Is-SCG-Maintenance"];
        
        if (isMaintenance) {
            [Utils showOnMaintenance:self serverDown:NO];
        } else {
            [Utils showOnMaintenance:self serverDown:YES];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSLog(@"%@", request.URL.absoluteString);
    NSString *scheme = request.URL.scheme;
    if ([scheme isEqualToString:@"popup"]) {
        NSString *absURLStr = [request.URL absoluteString];
        NSString *urlStr = [absURLStr stringByReplacingOccurrencesOfString:@"popup://ced?url=" withString:@""];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        return NO;
    }
    
    return YES;
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
}

@end
