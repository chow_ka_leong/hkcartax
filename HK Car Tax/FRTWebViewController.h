//
//  FRTWebViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 15/4/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "WebViewController.h"

@interface FRTWebViewController : WebViewController

- (IBAction)infoPressed;

@end
