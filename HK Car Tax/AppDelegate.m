//
//  AppDelegate.m
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "AppDelegate.h"
//#import <FacebookSDK/FacebookSDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "APIManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    //    [[UINavigationBar appearance] setTintColor:[UIColor lightGrayColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{UITextAttributeFont : [UIFont boldSystemFontOfSize:14]}];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:NO forKey:shallAllowLandscape];
    [userDefault synchronize];
    
    [self.window makeKeyAndVisible];
    
    [[Harpy sharedInstance] setAppID:APP_STORE_ID];
    [Harpy sharedInstance].countryCode = @"hk";
    [[Harpy sharedInstance] setPresentingViewController:_window.rootViewController];
    
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeNone];
    
    [[Harpy sharedInstance] setDebugEnabled:YES];

    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions] ;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[Harpy sharedInstance] setDelegate:self];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    APIManager *apiMgr = [APIManager sharedManager];
    [apiMgr getAccessToken:nil];
    [[Harpy sharedInstance] checkVersionMonthly];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}


#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        [self launchAppStore];
    }
}

- (void)harpyDidDetectNewVersionWithoutAlert:(NSString *)message {
    
    Language *lang = [Language sharedInstance];
    
    BOOL forceUpdate = [[Harpy sharedInstance] isForceUpdateVersion];
    
    NSString *title = [lang get:@"appUpdateMessage"];
    
    if (NSStringFromClass([UIAlertController class])) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:[lang get:@"update"]
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action){
                                                              [self launchAppStore];
                                                          }]];
        
//        if (!forceUpdate) {
//            [alertController addAction:[UIAlertAction actionWithTitle:[lang get:@"cancel"]
//                                                                style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction *action){
//                                                              }]];
//        }
        
        
        alertController.view.frame = [[UIScreen mainScreen] applicationFrame];
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    } else {
        
        UIAlertView *alert;
        
        
        alert = [[UIAlertView alloc] initWithTitle:title
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:forceUpdate ? nil : [lang get:@"cancel"]
                                 otherButtonTitles:[lang get:@"update"], nil];
        
        [alert show];
    }
}


- (void)launchAppStore
{
    NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", APP_STORE_ID];
    NSURL *iTunesURL = [NSURL URLWithString:iTunesString];
    [[UIApplication sharedApplication] openURL:iTunesURL];
    
}
@end
