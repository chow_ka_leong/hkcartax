//
//  SearchManager.h
//  HK Car Tax
//
//  Created by Rocky Wong on 19/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchManager : NSObject

@property (strong, nonatomic) NSMutableArray *makeList;

+ (id)sharedManager;

- (void)getMakeList:(void (^)(BOOL,  NSString *))callback;
- (void)getModelList:(void (^)(BOOL, NSString *))callback;

// search
- (void)search:(SearchQuery *)query
   saveHistory:(BOOL)save
      callback:(void (^)(NSArray *, NSString *))callback;

- (void)searchByModel:(ModelQuery *)modelQuery
          saveHistory:(BOOL)save
             callback:(void (^)(NSArray *, NSString *))callback;

- (void)searchByChassis:(ChassisQuery *)chassisQuery
            saveHistory:(BOOL)save
               callback:(void (^)(NSArray *, NSString *))callback;

- (void)searchByPRP:(PRPQuery *)prpQuery
        saveHistory:(BOOL)save
           callback:(void (^)(NSArray *, NSString *))callback;

// Utilities
- (NSArray *)getMakeStrList;
- (NSArray *)getModelStrList:(NSString *)makeName;
- (Make *)getMakeWithName:(NSString *)nameStr;
- (Model *)getModelWithName:(NSString *)nameStr make:(Make *)make;;

@end
