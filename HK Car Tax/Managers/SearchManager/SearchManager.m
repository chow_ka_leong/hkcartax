//
//  SearchManager.m
//  HK Car Tax
//
//  Created by Rocky Wong on 19/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "SearchManager.h"
#import "APIManager.h"
#import "HistoryManager.h"
#import "RequestHeader.h"

@implementation SearchManager

static NSString *const MAKE_VERSION_KEY = @"MAKE_VERSION";
static NSString *const MODEL_VERSION_KEY = @"MODEL_VERSION";

#pragma mark Singleton Methods

+ (id)sharedManager {
    static SearchManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

#pragma mark - GET

- (NSMutableDictionary *)getParams:(NSString *)versionKey
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    RequestHeader *reqHeader = [[RequestHeader alloc] init];
    reqHeader.version = [userDef integerForKey:versionKey];
    return [reqHeader getPostJSON];
}

- (void)getMakeList:(void (^)(BOOL, NSString *))callback
{
    APIManager *apiMgr = [APIManager sharedManager];
    NSMutableDictionary *body = [self getParams:MAKE_VERSION_KEY];
    
    [apiMgr postGetMakeList:body callback:^(NSData *json, NSHTTPURLResponse *response) {
        
        if ([Utils onServerTimeout:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(NO, [lang get:@"errorServerNotAvailable"]);
            }
        } else if ([Utils onMaintenance:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(NO, [lang get:@"errorServerMaintenance"]);
            }
            
        } else if (json) {
            NSError *error = nil;
            NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:json
                                                                       options:0
                                                                         error:&error];
//            TRACE(@"result: %@", resultDict);
            
            NSArray *ml = [resultDict objectForKey:@"make"];
            self.makeList = [NSMutableArray array];
            [ml enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary *dict = (NSDictionary *)obj;
                Make *m = [[Make alloc] initWithDict:dict];
                [self.makeList addObject:m];
            }];
            
            if (callback)
                callback(YES, nil);
        } else {
            if (callback)
                callback(NO, nil);
        }
    }];
}

- (void)getModelList:(void (^)(BOOL, NSString *))callback
{
    APIManager *apiMgr = [APIManager sharedManager];
    NSMutableDictionary *body = [self getParams:MAKE_VERSION_KEY];
    
    [apiMgr postGetModelList:body callback:^(NSData *json, NSHTTPURLResponse *response) {
        
        
        if ([Utils onServerTimeout:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(NO, [lang get:@"errorServerNotAvailable"]);
            }
        } else if ([Utils onMaintenance:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(NO, [lang get:@"errorServerMaintenance"]);
            }
            
        } else if (json) {
            NSError *error = nil;
            NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:json
                                                                       options:0
                                                                         error:&error];
//            TRACE(@"result: %@", resultDict);
            
            NSArray *ml = [resultDict objectForKey:@"make"];
            [ml enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary *dict = (NSDictionary *)obj;
                NSString *makeCode = [dict objectForKey:@"makeCode"];
                NSArray *modelList = [dict objectForKey:@"model"];
                Make *m = [self getMakeWithCode:makeCode];
                [m setModels:modelList];
            }];
            
            if (callback)
                callback(YES, nil);
        } else {
            if (callback)
                callback(NO, nil);
        }
        
    }];
}

- (Make *)getMakeWithCode:(NSString *)code
{
    for (Make *m in self.makeList) {
        if ([m.code isEqualToString:code])
            return m;
    }
    
    return nil;
}

#pragma mark - Picker

- (NSArray *)getMakeStrList
{
    __block NSMutableArray *ml = [NSMutableArray array];
    [self.makeList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Make *m = (Make *)obj;
        [ml addObject:m.name];
    }];
    
    return [NSArray arrayWithArray:ml];
}

- (NSArray *)getModelStrList:(NSString *)makeName
{
    NSArray *makeStrList = [self getMakeStrList];
    
    if (![makeStrList containsObject:makeName]) {
        return [NSArray array];
    }
    
    NSUInteger index = [makeStrList indexOfObject:makeName];
    Make *make = [self.makeList objectAtIndex:index];
    
    __block NSMutableArray *ml = [NSMutableArray array];
    
    [make.modelList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Model *model = (Model *)obj;
        [ml addObject:model.name];
    }];
    
    return [NSArray arrayWithArray:ml];
}

- (Make *)getMakeWithName:(NSString *)nameStr
{
    __block Make *result;
    
    [self.makeList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Make *m = (Make *)obj;
        if ([nameStr isEqualToString:m.name]) {
            result = m;
            *stop = YES;
        }
    }];
    
    return result;
}

- (Model *)getModelWithName:(NSString *)nameStr make:(Make *)make
{
    __block Model *result;
    
    [make.modelList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Model *m = (Model *)obj;
        if ([nameStr isEqualToString:m.name]) {
            result = m;
            *stop = YES;
        }
    }];
    
    return result;
}

#pragma mark - Search

- (void)search:(SearchQuery *)query saveHistory:(BOOL)save callback:(void (^)(NSArray *, NSString *))callback
{
    if ([query isKindOfClass:[ModelQuery class]]) {
        [self searchByModel:(ModelQuery *)query
                saveHistory:save
                   callback:callback];
    
    } else if ([query isKindOfClass:[ChassisQuery class]]) {
        [self searchByChassis:(ChassisQuery *)query
                  saveHistory:save
                     callback:callback];
    
    } else if ([query isKindOfClass:[PRPQuery class]]) {
        [self searchByPRP:(PRPQuery *)query
              saveHistory:save
                 callback:callback];
    }
}

- (void)searchByModel:(ModelQuery *)modelQuery saveHistory:(BOOL)save callback:(void (^)(NSArray *, NSString *))callback
{
    NSMutableDictionary *params = [modelQuery getQueryParams];
    
//    TRACE(@"param %@", params);
    
    if (save) {
        HistoryManager *historyMgr = [HistoryManager sharedManager];
        [historyMgr archiveQuery:modelQuery];
    }
    
    APIManager *apiMgr = [APIManager sharedManager];
    [apiMgr postSearch:params callback:^(NSData *json, NSHTTPURLResponse *response) {
        
        // maintenance
        if ([Utils onServerTimeout:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(nil, [lang get:@"errorServerNotAvailable"]);
            }
        } else if ([Utils onMaintenance:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(nil, [lang get:@"errorServerMaintenance"]);
            }
        
        } else if (json) {
            NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:json options:0 error:nil];
            NSString *errorStr = [Utils getObjectOrNil:[resultDict objectForKey:@"error"]];
            NSArray *resultList;
            
//            TRACE(@"%@", resultDict);
            
            // process prp list
            NSArray *prpList = [Utils getObjectOrNil:[resultDict objectForKey:@"prp"]];
            if (prpList) {
                NSMutableArray *list = [NSMutableArray array];
                [prpList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSDictionary *dict = (NSDictionary *)obj;
                    AutoDetail *detail = [[AutoDetail alloc] initWithDict:dict];
                    [list addObject:detail];
                }];
                
                resultList = [NSArray arrayWithArray:list];
            }
            
            if (callback)
                callback(resultList, errorStr);
        } else {
            if (callback)
                callback(nil, nil);
        }
    }];
}

- (void)searchByChassis:(ChassisQuery *)chassisQuery saveHistory:(BOOL)save callback:(void (^)(NSArray *, NSString *))callback
{
    NSMutableDictionary *params = [chassisQuery getQueryParams];
    
//    TRACE(@"param: %@", params);
    
    if (save) {
        HistoryManager *historyMgr = [HistoryManager sharedManager];
        [historyMgr archiveQuery:chassisQuery];
    }
    
    APIManager *apiMgr = [APIManager sharedManager];
    [apiMgr postSearch:params callback:^(NSData *json, NSHTTPURLResponse *response) {
        
        // maintenance
        if ([Utils onServerTimeout:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(nil, [lang get:@"errorServerNotAvailable"]);
            }
        } else if ([Utils onMaintenance:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(nil, [lang get:@"errorServerMaintenance"]);
            }
            
        } else if (json) {
            NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:json options:0 error:nil];
            NSString *errorStr = [Utils getObjectOrNil:[resultDict objectForKey:@"error"]];
            NSArray *resultList;
            
//            TRACE(@"%@", resultDict);
            
            // process prp list
            NSArray *prpList = [Utils getObjectOrNil:[resultDict objectForKey:@"prp"]];
            if (prpList) {
                NSMutableArray *list = [NSMutableArray array];
                [prpList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSDictionary *dict = (NSDictionary *)obj;
                    ChassisAutoDetail *detail = [[ChassisAutoDetail alloc] initWithDict:dict];
                    [list addObject:detail];
                }];
                
                resultList = [NSArray arrayWithArray:list];
            }
            
            if (callback)
                callback(resultList, errorStr);
            
        } else {
            if (callback)
                callback(nil, nil);
        }
    }];
}

- (void)searchByPRP:(PRPQuery *)prpQuery saveHistory:(BOOL)save callback:(void (^)(NSArray *, NSString *))callback
{
    NSMutableDictionary *params = [prpQuery getQueryParams];
    
//    TRACE(@"param: %@", params);
    
    if (save) {
        HistoryManager *historyMgr = [HistoryManager sharedManager];
        [historyMgr archiveQuery:prpQuery];
    }
    
    APIManager *apiMgr = [APIManager sharedManager];
    [apiMgr postSearch:params callback:^(NSData *json, NSHTTPURLResponse *response) {
        
        // maintenance
        if ([Utils onServerTimeout:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(nil, [lang get:@"errorServerNotAvailable"]);
            }
        } else if ([Utils onMaintenance:response]) {
            if (callback) {
                Language *lang = [Language sharedInstance];
                callback(nil, [lang get:@"errorServerMaintenance"]);
            }
        } else if (json) {
            NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:json options:0 error:nil];
            NSString *errorStr = [Utils getObjectOrNil:[resultDict objectForKey:@"error"]];
            NSArray *resultList;
            
//            TRACE(@"%@", resultDict);
            
            // process prp list
            NSArray *prpList = [Utils getObjectOrNil:[resultDict objectForKey:@"prp"]];
            if (prpList) {
                NSMutableArray *list = [NSMutableArray array];
                [prpList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSDictionary *dict = (NSDictionary *)obj;
                    PRPAutoDetail *detail = [[PRPAutoDetail alloc] initWithDict:dict];
                    [list addObject:detail];
                }];
                
                resultList = [NSArray arrayWithArray:list];
            }
            
            if (callback)
                callback(resultList, errorStr);
            
        } else {
            if (callback)
                callback(nil, nil);
        }
    }];
}



@end
