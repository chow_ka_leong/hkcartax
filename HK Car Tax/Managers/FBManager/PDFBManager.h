//
//  FBManager.h
//  MusicOne iPad
//
//  Created by developer on 5/4/13.
//  Copyright (c) 2013 Golden Dynamic Enterprises Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDFBManager : NSObject

#define kPDFacebookSSOScheme @"fb680223992040788"
#define kPDFacebookSSOURLScheme @"hkcustoms"

+ (id)sharedManager;

- (void)openSession;
- (void)clossSession;
- (void)shareTitle:(NSString *)titleStr body:(NSString *)bodyStr link:(NSString *)linkStr;

@end
