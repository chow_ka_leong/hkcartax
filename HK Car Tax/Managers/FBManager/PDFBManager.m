//
//  FBManager.m
//  MusicOne iPad
//
//  Created by developer on 5/4/13.
//  Copyright (c) 2013 Golden Dynamic Enterprises Ltd. All rights reserved.
//

//#import <FacebookSDK/FacebookSDK.h>
#import "PDFBManager.h"

typedef void (^OpenSessionCallback)(BOOL);

@interface PDFBManager ()

@property (copy, nonatomic) OpenSessionCallback osCallback;

@end

@implementation PDFBManager

+ (id)sharedManager
{
    static PDFBManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)openSession
{
    NSArray *permissions = @[@"basic_info"];
    
//    [FBSession openActiveSessionWithReadPermissions:permissions
//                                       allowLoginUI:YES
//                                  completionHandler:^(FBSession *session, FBSessionState statue, NSError *error) {
    
//                                      TRACE(@"status: %d", statue);
//                                      TRACE(@"other statue: %d", FBSession.activeSession.state);
            
        // if login fails for any reason, we alert
//        if (error) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:NSLocalizedString(@"Confirm", nil) otherButtonTitles:nil];
//            [alert show];
//            
//            if (self.osCallback)
//                self.osCallback(NO);
//
//        } else if (FB_ISSESSIONOPENWITHSTATE(statue)) {
            // send our requests if we successfully logged in
//            TRACE(@"login ok");
            
//            if (self.osCallback)
//                self.osCallback(YES);
//                
//        } else {
//            TRACE(@"unknown stats: %d", statue);
            
//            if (self.osCallback)
//                self.osCallback(NO);
//        }
//    }];
}

- (void)clossSession
{
//    [FBSession.activeSession closeAndClearTokenInformation];
}

- (void)shareTitle:(NSString *)titleStr body:(NSString *)bodyStr link:(NSString *)linkStr
{    
//    void (^shareLink)(BOOL) = ^(BOOL success) {
//        Language *lang = [Language sharedInstance];
//        
//        if (success) {
//            NSMutableDictionary *params = [NSMutableDictionary dictionary];
//            [params setObject:titleStr forKey:@"name"];
//            [params setObject:bodyStr forKey:@"description"];
//            [params setObject:linkStr forKey:@"link"];
//            
//            [FBWebDialogs presentFeedDialogModallyWithSession:nil parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//                
//                if (error) {
//                    [Utils showAlertWithTitle:[lang get:@"errorFbShareFailed"] message:nil];
//                    
//                } else if (result == FBWebDialogResultDialogCompleted) {
//                    NSDictionary *dict = [resultURL parseQuery];
//                    NSString *postID = [dict objectForKey:@"post_id"];
//                    if (postID)
//                        [Utils showAlertWithTitle:[lang get:@"fbShareSuccess"] message:nil];
//                }
//            }];
//        } else {
//            [Utils showAlertWithTitle:[lang get:@"errorFbLoginFailed"] message:nil];
//        }
//    };
    
//    if (FBSession.activeSession.isOpen) {
//        shareLink(YES);
//        
//    } else {
//        self.osCallback = shareLink;
//        [self openSession];
//    }
}


@end
