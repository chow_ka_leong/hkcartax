//
//  HistoryManager.m
//  HK Car Tax
//
//  Created by Rocky Wong on 24/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "HistoryManager.h"

@implementation HistoryManager

#pragma mark Singleton Methods

+ (id)sharedManager {
    static HistoryManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
        self.modelList = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getModelPath]];
        self.chassisList = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getChassisPath]];
        self.prpList = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getPRPPath]];
        
        if (self.modelList == nil) self.modelList = [NSMutableArray array];
        if (self.chassisList == nil) self.chassisList = [NSMutableArray array];
        if (self.prpList == nil) self.prpList = [NSMutableArray array];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

#pragma mark - File

- (NSString *)getModelPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    return [docDir stringByAppendingPathComponent:@"ModelHistory"];
}

- (NSString *)getChassisPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    return [docDir stringByAppendingPathComponent:@"ChassisHistory"];
}

- (NSString *)getPRPPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    return [docDir stringByAppendingPathComponent:@"PRPHistory"];
}

- (void)archiveQuery:(SearchQuery *)query
{
    NSMutableArray *historyList;
    
    if ([query isKindOfClass:[ModelQuery class]]) {
//        TRACE(@"model");
        historyList = self.modelList;
    } else if ([query isKindOfClass:[ChassisQuery class]]) {
//        TRACE(@"chassis");
        historyList = self.chassisList;
    } else {
//        TRACE(@"prp");
        historyList = self.prpList;
    }
    
    [historyList insertObject:query atIndex:0];
    if ([historyList count] > 5)
        [historyList removeLastObject];
    
    [self archive];
}

- (void)archive
{
    [NSKeyedArchiver archiveRootObject:self.modelList toFile:[self getModelPath]];
    [NSKeyedArchiver archiveRootObject:self.chassisList toFile:[self getChassisPath]];
    [NSKeyedArchiver archiveRootObject:self.prpList toFile:[self getPRPPath]];
}


@end
