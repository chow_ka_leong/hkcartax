//
//  HistoryManager.h
//  HK Car Tax
//
//  Created by Rocky Wong on 24/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryManager : NSObject

@property (strong, nonatomic) NSMutableArray *modelList;
@property (strong, nonatomic) NSMutableArray *chassisList;
@property (strong, nonatomic) NSMutableArray *prpList;

+ (id)sharedManager;

- (void)archiveQuery:(SearchQuery *)query;
- (void)archive;

@end
