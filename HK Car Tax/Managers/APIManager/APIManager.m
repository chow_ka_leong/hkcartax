//
//  APIManager.m
//  HK Car Tax
//
//  Created by Rocky Wong on 18/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "APIManager.h"
#import "FBEncryptorAES.h"

@implementation NSURLRequest (NSURLRequestWithIgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}

@end

@implementation APIManager

#pragma mark Singleton Methods

+ (id)sharedManager {
    static APIManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

#pragma mark - Access Token

- (NSMutableURLRequest *)accessTokenRequest
{
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionary];
    [requestParams setObject:APP_NAME forKey:@"appName"];
    [requestParams setObject:[Utils appVersion] forKey:@"appVer"];
    [requestParams setObject:@"ios" forKey:@"osType"];
    [requestParams setObject:[Utils systemVersion] forKey:@"osVer"];
    [requestParams setObject:[Utils deviceModel] forKey:@"deviceModel"];
    [requestParams setObject:[Utils deviceIdentifier] forKey:@"deviceIdentifier"];
    
    NSURL *url = [NSURL URLWithString:GET_ACCESS_TOKEN_URL];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:REQUEST_TIMEOUT];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDate *date = [NSDate date];
//    NSDate *date = [NSDate dateWithTimeIntervalSince1970:1000];
    NSString *sourceStr = [NSString stringWithFormat:@"%@|ios|%d", ACCESS_TOKEN_CHECK_VALUE, (int)[date timeIntervalSince1970]];
    NSString *encrypted = [FBEncryptorAES encryptBase64String:sourceStr keyString:ACCESS_TOKEN_ENCRYPT_KEY separateLines:NO];
    
    if (encrypted) {
        [request setValue:encrypted forHTTPHeaderField:@"k"];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestParams options:0 error:nil];
        [request setHTTPBody:jsonData];
        return request;
        
    } else {
        return nil;
    }
}

- (void)getAccessTokenOnStart:(void (^)(NSInteger, NSString *errStr))callback
{
    NSInteger (^getToken)() = ^{
        NSMutableURLRequest *request = [self accessTokenRequest];
        NSInteger statusCode;
        NSHTTPURLResponse *response;
        NSData *data = [self sendRequest:request statusCode:&statusCode response:&response];
        
        //        TRACE(@"statusCode: %d", statusCode);
        //        TRACE(@"data: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        if ([Utils onServerTimeout:response]) {
            statusCode = -1;
            return statusCode;
        } else if ([Utils onMaintenance:response]) {
            statusCode = -2;
            return statusCode;
        }
        
        if (data) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString *tokenStr = [responseDict objectForKey:@"token"];
            //            TRACE("accessToken: %@", tokenStr);
            self.accessToken = tokenStr;
            return statusCode;
            
        } else {
            return statusCode;
        }
    };
    
    if ([NSThread isMainThread]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSInteger statusCode = getToken();
            if (callback) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    if (statusCode == -1) {
                        Language *lang = [Language sharedInstance];
                        callback(0, [lang get:@"errorServerNotAvailable"]);
                        
                    } else if (statusCode == -2) {
                        Language *lang = [Language sharedInstance];
                        callback(0, [lang get:@"errorServerMaintenance"]);
                    } else {
                        callback(statusCode, nil);
                    }
                });
            }
        });
    } else {
        NSInteger statusCode = getToken();
        if (callback) {
            callback(statusCode, nil);
        }
    }
}

- (void)getAccessToken:(void (^)(NSInteger))callback
{
    NSInteger (^getToken)() = ^{
        NSMutableURLRequest *request = [self accessTokenRequest];
        NSInteger statusCode;
        NSHTTPURLResponse *response = nil;
        NSData *data = [self sendRequest:request statusCode:&statusCode response:&response];
        
//        TRACE(@"statusCode: %d", statusCode);
//        TRACE(@"data: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        if (data) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString *tokenStr = [responseDict objectForKey:@"token"];
//            TRACE("accessToken: %@", tokenStr);
            self.accessToken = tokenStr;
            return statusCode;
            
        } else {
            return statusCode;
        }
    };
    
    if ([NSThread isMainThread]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSInteger statusCode = getToken();
            if (callback) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    callback(statusCode);
                });
            }
        });
    } else {
        NSInteger statusCode = getToken();
        if (callback) {
            callback(statusCode);
        }
    }
}

- (void)getAuthRequest:(NSString *)urlStr callback:(void (^)(NSMutableURLRequest *))callback
{
    void (^getRequest)() = ^{
        if (self.accessToken) {
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
            NSLog(urlStr);
            [request setValue:self.accessToken forHTTPHeaderField:@"t"];
            callback(request);
            
        } else {
            callback(nil);
        }
    };
     
    if (self.accessToken == nil) {
        [self getAccessToken:^(NSInteger statusCode) {
            getRequest();
        }];
    } else {
        getRequest();
    }
}

#pragma mark - GET

- (void)getFromURL:(NSURL *)url callback:(void (^)(NSData *,  NSHTTPURLResponse *))callback
{
    void (^getData)() = ^{
        if ([NSThread isMainThread]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self getFromURL:url
                     cachePolicy:NSURLRequestUseProtocolCachePolicy
                        callback:^(NSData *data, NSHTTPURLResponse *response) {
                        if (callback) {
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                callback(data, response);
                            });
                        }
                }];
            });
        } else {
            [self getFromURL:url
                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                    callback:^(NSData *data, NSHTTPURLResponse *response) {
                        if (callback) {
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                callback(data, response);
                            });
                        }
                    }];
        }
    };
    
    if (self.accessToken == nil) {
        [self getAccessToken:^(NSInteger statusCode) {
            getData();
        }];
    } else {
        getData();
    }
}


#pragma mark - POST

- (void)postGetAnnouncement:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback
{
    NSURL *url = [NSURL URLWithString:POST_TO_GET_ANNOUNCEMENT];
    NSMutableURLRequest *request = [self requestForURL:url postBody:postBody];
    [self postRequest:request callback:callback];
}

- (void)postGetMakeList:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback
{
    NSURL *url = [NSURL URLWithString:POST_TO_GET_MAKE_LIST_URL];
    NSMutableURLRequest *request = [self requestForURL:url postBody:postBody];
    [self postRequest:request callback:callback];
}

- (void)postGetModelList:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback
{
    NSURL *url = [NSURL URLWithString:POST_TO_GET_MODEL_LIST_URL];
    NSMutableURLRequest *request = [self requestForURL:url postBody:postBody];
    [self postRequest:request callback:callback];
}

- (void)postSearch:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback
{
    NSURL *url = [NSURL URLWithString:POST_SEARCH_URL];
    NSMutableURLRequest *request = [self requestForURL:url postBody:postBody];
    [self postRequest:request callback:callback];
}

- (void)postCheckAppUpgrade:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback
{
    NSURL *url = [NSURL URLWithString:POST_TO_CHECK_UPGRADE_URL];
    NSMutableURLRequest *request = [self requestForURL:url postBody:postBody];
    [self postRequest:request callback:callback];
}

#pragma mark - Helpers

- (NSURL *)getLanguageURL:(NSString *)urlStr
{
    Language *lang = [Language sharedInstance];
    NSString *langStr = [lang currentLanguage];
    if ([langStr isEqualToString:@"Base"]) {
        urlStr = [urlStr stringByReplacingOccurrencesOfString:@":lang" withString:@"chi"];
    } else {
        urlStr = [urlStr stringByReplacingOccurrencesOfString:@":lang" withString:@"eng"];
    }
    return [NSURL URLWithString:urlStr];
}

- (NSMutableURLRequest *)requestForURL:(NSURL *)url method:(NSString *)method
{
    return [self requestForURL:url parameters:[NSDictionary dictionary] method:method cachePolicy:NSURLRequestUseProtocolCachePolicy];
}

- (NSMutableURLRequest *)requestForURL:(NSURL *)url parameters:(NSDictionary *)parameters method:(NSString *)method cachePolicy:(NSURLRequestCachePolicy)cachePolicy
{
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:parameters];
    NSURL *requestURL = [self replaceParameters:requestParams inURL:url];
    NSMutableURLRequest *request = nil;
    
//    TRACE(@"%@", requestURL);
    
    if ([method isEqualToString:@"GET"]) {
        NSString *queryString = [self queryStringFromParameters:requestParams];
        requestURL = [requestURL URLByAppendingQueryString:queryString];
        request = [NSMutableURLRequest requestWithURL:requestURL cachePolicy:cachePolicy timeoutInterval:REQUEST_TIMEOUT];
    } else if ([method isEqualToString:@"POST"]) {
        request = [[NSMutableURLRequest alloc] initWithURL:requestURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:REQUEST_TIMEOUT];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setFormPostParameters:requestParams];
    }
    // add accessToken to all except Get Access Token API
//    TRACE(@"accessToken: %@", self.accessToken);
    
    if (self.accessToken)
        [request setValue:self.accessToken forHTTPHeaderField:@"t"];
    
    return request;
}

- (NSMutableURLRequest *)requestForURL:(NSURL *)url postBody:(NSDictionary *)postBody
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:REQUEST_TIMEOUT];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
//    TRACE(@"%@", url);
    
    if (postBody) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postBody options:0 error:nil];
        [request setHTTPBody:jsonData];
    }
    
    // add accessToken to all except Get Access Token API
    if (self.accessToken) {
        [request setValue:self.accessToken forHTTPHeaderField:@"t"];
    }
    
    return request;
}

- (void)postRequestProcess:(NSURLRequest *)request callback:(void (^)(NSData *, NSHTTPURLResponse *))callback
{
    __block NSError *error = nil;
    __block NSHTTPURLResponse *response = nil;
    
    void (^postRequest)() = ^{
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error) {
            NSLog(@"[ERROR] Failed POST request to %@: %@", [request URL], [error localizedDescription]);
            dispatch_sync(dispatch_get_main_queue(), ^{
                callback(data, response);
            });
        } else {
            // access token expired
            if ([response statusCode] == 419)
                self.accessToken = nil;
            
            if (callback) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    callback(data, response);
                });
            }
        }
    };
    
    if ([NSThread isMainThread]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            postRequest();
        });
    } else {
        postRequest();
    }
}

- (void)postRequest:(NSMutableURLRequest *)request callback:(void (^)(NSData *, NSHTTPURLResponse *))callback
{
    if (self.accessToken == nil) {
        [self getAccessToken:^(NSInteger statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                self.invalidHandsetTime = NO;
                [request setValue:self.accessToken forHTTPHeaderField:@"t"];
                [self postRequestProcess:request callback:callback];
                
            } else {
                self.invalidHandsetTime = (statusCode == 403);
                self.requestTimedOut = (statusCode == 0);
                callback(nil, nil);
            }
        }];
        
    } else {
        [self postRequestProcess:request callback:callback];
    }
}


// get access token
- (NSData *)sendRequest:(NSURLRequest *)request statusCode:(NSInteger *)statusCode response:(NSHTTPURLResponse **)response
{
    NSError *error = nil;
    NSHTTPURLResponse *res = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&res error:&error];
    *statusCode = [res statusCode];
    *response = res;
//    TRACE(@"code: %d", *statusCode);
    
    if (error) {
        NSLog(@"[ERROR] Failed POST request to %@: %@", [request URL], [error localizedDescription]);
        return nil;
    } else if (*statusCode >= 200 && *statusCode < 300) {
        return data;
    } else {
        NSLog(@"[ERROR] Failed POST request to %@: response was %ld %@",
              [request URL],
              (long)[res statusCode],
              [NSHTTPURLResponse localizedStringForStatusCode:[res statusCode]]);
        return nil;
    }
}

- (void)getFromURL:(NSURL *)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy callback:(void (^)(NSData *, NSHTTPURLResponse *))callback
{
    NSError *error = nil;
    NSHTTPURLResponse *response = nil;
    NSURLRequest *request = [self requestForURL:url parameters:[NSDictionary dictionary] method:@"GET" cachePolicy:cachePolicy];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSInteger statusCode = [response statusCode];
    
    if (error) {
        NSLog(@"[ERROR] Failed GET request to %@: %@", [request URL], [error localizedDescription]);
        self.accessToken = nil;          // assume access token expired on error
        callback(nil, response);
        
    } else if (statusCode == 200) {
        callback(data, response);
        
    } else {
        NSLog(@"[ERROR] Failed GET request to %@: response was %ld %@",
              [request URL],
              (long)statusCode,
              [NSHTTPURLResponse localizedStringForStatusCode:[response statusCode]]);
        self.accessToken = nil;          // assume access token expired on error
        callback(nil, response);
    }
}

- (NSURL *)replaceParameters:(NSMutableDictionary *)parameters inURL:(NSURL *)url {
    __block NSMutableString *urlString = [NSMutableString stringWithString:[url absoluteString]];
    NSDictionary *allParameters = [NSDictionary dictionaryWithDictionary:parameters];
    [allParameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *keyToReplace = [@":" stringByAppendingString:key];
        NSRange range = [urlString rangeOfString:keyToReplace options:NSCaseInsensitiveSearch];
        if (range.location != NSNotFound) {
            [urlString replaceCharactersInRange:range withString:obj];
            [parameters removeObjectForKey:key];
        }
    }];
    return [NSURL URLWithString:urlString];
}

- (NSString *)queryStringFromParameters:(NSDictionary *)parameters {
    __block NSMutableString *queryString = [NSMutableString stringWithString:@""];
    if ([parameters count] > 0) {
        [parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSString *queryParameter = [NSString stringWithFormat:@"%@=%@&", key, obj];
            [queryString appendString:queryParameter];
        }];
        // Remove the last "&"
        [queryString deleteCharactersInRange:NSMakeRange([queryString length] - 1, 1)];
    }
    return queryString;
}

#pragma mark - APNS

- (NSData *)postParams:(NSDictionary *)params toURL:(NSURL *)url
{
    NSError *error = nil;
    NSHTTPURLResponse *response = nil;
    NSURLRequest *request = [self requestForURL:url parameters:params method:@"POST" cachePolicy:NSURLRequestUseProtocolCachePolicy];
    
//    TRACE(@"%@", params);
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
//    TRACE(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    
    if (error) {
        NSLog(@"[ERROR] Failed POST request to %@: %@", [request URL], [error localizedDescription]);
        return nil;
    } else if ([response statusCode] == 200) {
        return data;
    } else {
        NSLog(@"[ERROR] Failed POST request to %@: response was %ld %@",
              [request URL],
              (long)[response statusCode],
              [NSHTTPURLResponse localizedStringForStatusCode:[response statusCode]]);
        return nil;
    }
}

@end
