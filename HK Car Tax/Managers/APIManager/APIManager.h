//
//  APIManager.h
//  HK Car Tax
//
//  Created by Rocky Wong on 18/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIManager : NSObject

@property (copy, nonatomic) NSString *accessToken;
@property (nonatomic) BOOL invalidHandsetTime;
@property (nonatomic) BOOL requestTimedOut;

+ (id)sharedManager;

#pragma mark - Access Token

- (void)getAccessToken:(void (^)(NSInteger))callback;
- (void)getAccessTokenOnStart:(void (^)(NSInteger, NSString *errStr))callback;
- (void)getAuthRequest:(NSString *)urlStr callback:(void (^)(NSMutableURLRequest *))callback;

#pragma mark - POST

- (void)postGetAnnouncement:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback;
- (void)postGetMakeList:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback;
- (void)postGetModelList:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback;
- (void)postSearch:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback;
- (void)postCheckAppUpgrade:(NSMutableDictionary *)postBody callback:(void (^)(NSData *, NSHTTPURLResponse *))callback;

@end
