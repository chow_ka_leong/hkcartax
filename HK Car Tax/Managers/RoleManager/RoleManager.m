//
//  RoleManager.m
//  HK Car Tax
//
//  Created by Rocky Wong on 27/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "RoleManager.h"
#import "APIManager.h"

@implementation RoleManager

#pragma mark Singleton Methods

+ (id)sharedManager {
    static RoleManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (BOOL)isDisclaimerAccepted
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    return [userDef boolForKey:@"disclaimerAccepted"];
}

- (void)acceptDisclaimer
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setBool:YES forKey:@"disclaimerAccepted"];
    [userDef synchronize];
}

- (NSString *)getResourceStr:(NSString *)sourceStr
{
    if (self.isTrader)
        return [sourceStr stringByAppendingString:@"_trader"];
    else
        return [sourceStr stringByAppendingString:@"_buyer"];
}

- (NSString *)getRoleURLStr:(NSString *)urlStr
{
    NSString *roleStr = self.isTrader ? @"T" : @"B";
    NSString *langStr;
    Language *lang = [Language sharedInstance];
    if ([lang tc])
        langStr = @"tc";
    else if ([lang sc])
        langStr = @"sc";
    else
        langStr = @"en";

    return [urlStr stringByAppendingFormat:@"?userType=%@&lang=%@", roleStr, langStr];
}

- (void)getAnnouncement:(void (^)(NSString *, BOOL, BOOL, BOOL))callback
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.isTrader)
        [params setObject:@"TAN" forKey:@"textType"];
    else
        [params setObject:@"BAN" forKey:@"textType"];

    Language *lang = [Language sharedInstance];
    if ([lang tc])
        [params setObject:@"TC" forKey:@"lang"];
    else if ([lang sc])
        [params setObject:@"SC" forKey:@"lang"];
    else
        [params setObject:@"EN" forKey:@"lang"];
    
    APIManager *apiMgr = [APIManager sharedManager];
    [apiMgr postGetAnnouncement:params callback:^(NSData *json, NSHTTPURLResponse *response) {
        
        if (json) {
            NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:json options:0 error:nil];
            NSString *contentStr = [Utils getObjectOrNil:[resultDict objectForKey:@"content"]];
            if (callback) {
                NSString *returnStr = contentStr ? contentStr : @"";
                callback(returnStr, [Utils onMaintenance:response], NO, [Utils onServerTimeout:response]);
            }
        } else if (response) {
            if (callback)
                callback(nil, [Utils onMaintenance:response], NO, [Utils onServerTimeout:response]);
        } else {
            if (callback)
                callback(nil, NO, apiMgr.invalidHandsetTime, YES);
        }
    }];
}


@end
