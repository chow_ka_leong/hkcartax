//
//  RoleManager.h
//  HK Car Tax
//
//  Created by Rocky Wong on 27/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoleManager : NSObject

@property (nonatomic) BOOL isTrader;

+ (id)sharedManager;

- (BOOL)isDisclaimerAccepted;
- (void)acceptDisclaimer;
- (NSString *)getRoleURLStr:(NSString *)urlStr;
- (NSString *)getResourceStr:(NSString *)sourceStr;
- (void)getAnnouncement:(void (^)(NSString *contentStr, BOOL maintenance, BOOL invalidTime, BOOL timeout))callback;

@end
