//
//  VideoManager.m
//  HK Car Tax
//
//  Created by Rocky Wong on 24/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "VideoManager.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>  

@interface VideoManager () <NSURLConnectionDelegate>

@property (nonatomic) unsigned long long expectedTotalBytes;

@end

@implementation VideoManager

#pragma mark Singleton Methods

+ (id)sharedManager {
    static VideoManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

- (NSString *)getFilePath
{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    NSString *filename = [VIDEO_URL lastPathComponent];
//    return [docDir stringByAppendingPathComponent:filename];
    return [[NSBundle mainBundle] pathForResource:@"api_video" ofType:@"mp4"];
}

- (BOOL)videoExists
{
//    NSFileManager *fileMgr = [NSFileManager defaultManager];
//    TRACE(@"path: %@", [self getFilePath]);
//    return [fileMgr fileExistsAtPath:[self getFilePath]];
    return YES;
}

- (void)download
{
    self.hud = [MBProgressHUD showHUDAddedTo:self.parentViewCtrl.view animated:YES];
    
    NSURL *url = [NSURL URLWithString:VIDEO_URL];
//    TRACE(@"url: %@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [connection start];
    
}

- (void)play
{
    NSURL *url = [NSURL fileURLWithPath:[self getFilePath]];
    
    AVPlayerViewController * _moviePlayer1 = [[AVPlayerViewController alloc] init];
    _moviePlayer1.player = [AVPlayer playerWithURL:url];
    
    [self.parentViewCtrl presentViewController:_moviePlayer1 animated:YES completion:^{
        [_moviePlayer1.player play];
    }];
    
    //MPMoviePlayerViewController *moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
    //[self.parentViewCtrl presentMoviePlayerViewControllerAnimated:moviePlayerController];
    //[moviePlayerController.moviePlayer play];
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response
{
    NSInteger statusCode = [response statusCode];
    if (statusCode == 200) {
        self.expectedTotalBytes = [response expectedContentLength];
        
        if (self.expectedTotalBytes != NSURLResponseUnknownLength)
            self.hud.mode = MBProgressHUDModeDeterminate;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSString *path = [self getFilePath];
    NSFileHandle *hFile = [NSFileHandle fileHandleForWritingAtPath:path];
    
    if (!hFile) {
        [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
        hFile = [NSFileHandle fileHandleForWritingAtPath:path];
    }
    
    //did we finally get an accessable file?
    if (!hFile)
    {   //nope->bomb out!
        NSLog(@"could not write to file %@", path);
        return;
    }
    //we never know - hence we better catch possible exceptions!
    @try
    {
        //seek to the end of the file
        unsigned long long currentSize = [hFile seekToEndOfFile];
        //finally write our data to it
        [hFile writeData:data];
        
        currentSize += [data length];
        
        self.hud.progress = currentSize / (double)self.expectedTotalBytes;
    }
    @catch (NSException * e)
    {
        NSLog(@"exception when writing to file %@", path);
    }
    [hFile closeFile];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self.hud removeFromSuperview];
    [self play];
}

#pragma mark - Skip HTTPS

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

#pragma mark - Rotation

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}


@end
