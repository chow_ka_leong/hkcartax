//
//  VideoManager.h
//  HK Car Tax
//
//  Created by Rocky Wong on 24/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoManager : NSObject

@property (weak, nonatomic) MBProgressHUD *hud;
@property (weak, nonatomic) UIViewController *parentViewCtrl;

+ (id)sharedManager;

- (BOOL)videoExists;
- (void)download;
- (void)play;

@end
