//
//  AutoItemTableCell.h
//  HK Car Tax
//
//  Created by Rocky Wong on 9/5/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoItemTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;

@end
