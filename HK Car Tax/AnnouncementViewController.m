//
//  AnnouncementViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 27/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "AnnouncementViewController.h"
#import "MenuViewController.h"

@interface AnnouncementViewController ()

@end

@implementation AnnouncementViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        Language *lang = [Language sharedInstance];
        self.navigationItem.title = [lang get:@"appName"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Language *lang = [Language sharedInstance];
    
    UIImage *blueImage = [[UIImage imageNamed:@"btn_blue.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    [self.skipBtn setBackgroundImage:blueImage forState:UIControlStateNormal];
    [self.skipBtn setTitle:[lang get:@"skip"] forState:UIControlStateNormal];
    
    self.textView.text = self.contentStr;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
*/

- (IBAction)skipPressed
{
    MenuViewController *menuCtrl = [Utils getStoryboardViewController:@"MenuViewController"];
    
    NSMutableArray *navCtrlViews = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    [navCtrlViews replaceObjectAtIndex:navCtrlViews.count - 1 withObject:menuCtrl];
    
    [self.navigationController setViewControllers:navCtrlViews animated:YES];
    
//    [self.navigationController pushViewController:menuCtrl animated:YES];
}

@end
