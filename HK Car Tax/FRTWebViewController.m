//
//  FRTWebViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 15/4/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "FRTWebViewController.h"
#import "APIManager.h"

@interface FRTWebViewController ()

@end

@implementation FRTWebViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Language *lang = [Language sharedInstance];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"questionMark.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self action:@selector(infoPressed)];
    self.navigationItem.rightBarButtonItem.accessibilityLabel = [lang get:@"remark"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)infoPressed
{
    Language *lang = [Language sharedInstance];
    
    NSString * url_Str = @"";
    
    if([lang en]){
        url_Str = CALCULATOR_INFO_EN_URL;
    }
    else if([lang tc]){
        url_Str = CALCULATOR_INFO_TC_URL;
    }
    else{
        url_Str = CALCULATOR_INFO_SC_URL;
    }
    
    
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    APIManager *api = [APIManager sharedManager];
    [api getAuthRequest:url_Str callback:^(NSMutableURLRequest *req) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if (req) {
            WebViewController *webViewCtrl = [[WebViewController alloc] init];
            webViewCtrl.request = req;
            [self.navigationController pushViewController:webViewCtrl animated:YES];
        }
    }];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    BOOL res = [super webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
    
    if (!res) {
        return res;
    }
    
    NSString *scheme = request.URL.scheme;
    if ([scheme isEqualToString:@"cartax"]) {
        NSString *host = request.URL.host;
        
        if ([host isEqualToString:@"backtoprp"]) {
            [self.navigationController popViewControllerAnimated:YES];
            return NO;
        }
    }
    
    return YES;
}

@end
