//
//  WebViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 24/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) UIWebView *webView;
@property (strong, nonatomic) NSMutableURLRequest *request;

@end
