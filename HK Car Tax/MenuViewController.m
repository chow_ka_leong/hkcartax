//
//  MenuViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "MenuViewController.h"
#import "WebViewController.h"
#import "ExternalWebViewController.h"
#import "FRTWebViewController.h"
#import "VideoManager.h"
#import "AnnouncementViewController.h"
//#import "PDFBManager.h"
#import "APIManager.h"
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface MenuViewController () <UIActionSheetDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) NSArray *btnStrList;
@property (strong, nonatomic) NSArray *indexList;

@end

@implementation MenuViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        Language *lang = [Language sharedInstance];
        self.title = [lang get:@"appName"];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"menu" ofType:@"plist"];
        self.btnStrList = [NSArray arrayWithContentsOfFile:path];
        self.indexList = @[@(0), @(1), @(2), @(3), @(4), @(5), @(6), @(7)];
        
        // video expiry
        NSDate *today = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *expiryDate = [formatter dateFromString:VIDEO_EXPIRY_DATE];
        
        if ([today compare:expiryDate] == NSOrderedDescending) {
            NSMutableArray *btnList = [NSMutableArray arrayWithArray:self.btnStrList];
            [btnList removeObjectAtIndex:3];
            self.btnStrList = [NSArray arrayWithArray:btnList];
            self.indexList = @[@(0), @(1), @(2), @(4), @(5), @(6), @(7)];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Language *lang = [Language sharedInstance];
    self.navigationItem.rightBarButtonItem.accessibilityLabel = [lang get:@"share"];
//    self.navigationItem.hidesBackButton = YES;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.btnStrList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Language *lang = [Language sharedInstance];
    RoleManager *roleMgr = [RoleManager sharedManager];
    
    NSString *keyStr = [self.btnStrList objectAtIndex:indexPath.row];
    NSString *roleKeyStr = [roleMgr getResourceStr:keyStr];
    
    if ([keyStr isEqualToString:@"menu_btn_swap"]) {
        roleKeyStr = [keyStr stringByAppendingString:(roleMgr.isTrader) ? @"_buyer": @"_trader"];
    }
    
    cell.accessibilityLabel = [lang get:roleKeyStr];
    
    //    TRACE(@"label: %@", cell.accessibilityLabel);
    
    NSString *imageStr;
    
    if ([lang tc])
        imageStr = [roleKeyStr stringByAppendingString:@"_tc.png"];
    else if ([lang sc])
        imageStr = [roleKeyStr stringByAppendingString:@"_sc.png"];
    else
        imageStr = [roleKeyStr stringByAppendingString:@"_en.png"];
    
    UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:1001];
    imageView.image = [UIImage imageNamed:imageStr];
        
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (![Utils isServerReachable]) {
        [Utils showNotReachable:self];
        return;
    }
    
    RoleManager *roleMgr = [RoleManager sharedManager];
    NSString *keyStr = [self.btnStrList objectAtIndex:indexPath.row];
    NSString *roleKeyStr = [roleMgr getResourceStr:keyStr];
    Language *lang = [Language sharedInstance];
    NSString *titleStr = [lang get:roleKeyStr];
    
    if ([keyStr isEqualToString:@"menu_btn_note"]) {
        NSString *urlStr = roleMgr.isTrader ? NOTE_TO_TRADER_URL : NOTE_TO_BUYER_URL;
        
        if(!roleMgr.isTrader){
            
            if([lang en]){
                [self pushWebViewCtrl:NOTE_TO_BUYER_EN_URL title:titleStr];
            }
            else if([lang tc]){
                [self pushWebViewCtrl:NOTE_TO_BUYER_TC_URL title:titleStr];
            }
            else{
                [self pushWebViewCtrl:NOTE_TO_BUYER_SC_URL title:titleStr];
            }
            
        }
        else{
            [self pushWebViewCtrl:NOTE_TO_TRADER_URL title:titleStr];
        }
        
        
    } else if ([keyStr isEqualToString:@"menu_btn_cal"]) {
        if([lang en]){
            [self pushFRTWebViewCtrl:CALCULATOR_EN_URL title:titleStr];
        }
        else if([lang tc]){
            [self pushFRTWebViewCtrl:CALCULATOR_TC_URL title:titleStr];
        }
        else{
            [self pushFRTWebViewCtrl:CALCULATOR_SC_URL title:titleStr];
        }

        
    } else if ([keyStr isEqualToString:@"menu_btn_search"]) {
        [self performSegueWithIdentifier:@"searchSegue" sender:self];
        
    } else if ([keyStr isEqualToString:@"menu_btn_video"]) {
        [self processVideo];
        
    } else if ([keyStr isEqualToString:@"menu_btn_game"]) {
        [self pushWebViewCtrl:QUIZ_URL title:titleStr];
        
    } else if ([keyStr isEqualToString:@"menu_btn_glossary"]) {
        [self pushWebViewCtrl:GLOSSARY_URL title:titleStr];
        
    } else if ([keyStr isEqualToString:@"menu_btn_link"]) {
        [self pushExtWebViewCtrl:LINK_TO_OTHERS_URL title:titleStr];
        
    } else if ([keyStr isEqualToString:@"menu_btn_swap"]) {
        [self roleSwapPressed];
    }
}

- (void)pushWebViewCtrl:(NSString *)urlStr title:(NSString *)titleStr
{
    // managers
    RoleManager *roleMgr = [RoleManager sharedManager];
    APIManager *api = [APIManager sharedManager];
    
    // WebView
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    NSString *outrStr = [roleMgr getRoleURLStr:urlStr];
        TRACE(@"url: %@", outrStr);
    [api getAuthRequest:[self appendTimestamp2:outrStr] callback:^(NSMutableURLRequest *req) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if (req) {
            [[NSURLCache sharedURLCache] removeAllCachedResponses];
            [[NSURLCache sharedURLCache] setDiskCapacity:0];
            [[NSURLCache sharedURLCache] setMemoryCapacity:0];
            
            NSHTTPCookieStorage * cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage;
            for(NSHTTPCookie * cookie in cookies.cookies){
                NSLog(@"Cookies : ");
                
                [cookies deleteCookie:cookie];
            }
            
            UIViewController * vc = [[UIViewController alloc] init];
            UIWebView * wv = [[UIWebView alloc] initWithFrame:vc.view.bounds];
            [vc.view addSubview:wv];
            [wv loadRequest:req];
            vc.title = titleStr;
            [self.navigationController pushViewController:vc animated:YES];
            
            
            
//            WebViewController *webViewCtrl = [[WebViewController alloc] init];
//            webViewCtrl.request = req;
//            webViewCtrl.title = titleStr;
//            [self.navigationController pushViewController:webViewCtrl animated:YES];
        }
    }];
}

- (void)pushExtWebViewCtrl:(NSString *)urlStr title:(NSString *)titleStr
{
    RoleManager *roleMgr = [RoleManager sharedManager];
    NSString *outrStr = [roleMgr getRoleURLStr:urlStr];
//    urlStr = [urlStr stringByReplacingOccurrencesOfString:@"eservices.customs.gov.hk" withString:@"eservices0.customs.gov.hk"];
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    APIManager *api = [APIManager sharedManager];
    [api getAuthRequest:[self appendTimestamp:outrStr] callback:^(NSMutableURLRequest *req) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if (req) {
            ExternalWebViewController *webViewCtrl = [[ExternalWebViewController alloc] init];
            webViewCtrl.request = req;
            webViewCtrl.title = titleStr;
            [self.navigationController pushViewController:webViewCtrl animated:YES];
        }
    }];
}

- (void)pushFRTWebViewCtrl:(NSString *)urlStr title:(NSString *)titleStr
{
    RoleManager *roleMgr = [RoleManager sharedManager];
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    APIManager *api = [APIManager sharedManager];
    NSString *outrStr = [roleMgr getRoleURLStr:urlStr];
    [api getAuthRequest:[self appendTimestamp:outrStr] callback:^(NSMutableURLRequest *req) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if (req) {
            FRTWebViewController *webViewCtrl = [[FRTWebViewController alloc] init];
            webViewCtrl.request = req;
            webViewCtrl.title = titleStr;
            [self.navigationController pushViewController:webViewCtrl animated:YES];
        }
    }];
}

-(NSString *)appendTimestamp:(NSString *)url {
    return url;//[url stringByAppendingString:[NSString stringWithFormat:@"?__timestamp=%d", (int)[[NSDate date] timeIntervalSince1970]]];
}

-(NSString *)appendTimestamp2:(NSString *)url {
    return [url stringByAppendingString:[NSString stringWithFormat:@"&__timestamp=%d", (int)[[NSDate date] timeIntervalSince1970]]];
}

- (void)processVideo
{
    VideoManager *videoMgr = [VideoManager sharedManager];
    if ([videoMgr videoExists]) {
        videoMgr.parentViewCtrl = self.navigationController;
        [videoMgr play];
        
    } else {
        videoMgr.parentViewCtrl = self.navigationController;
        [videoMgr download];
    }
}

- (void)roleSwapPressed
{
    if (![Utils isServerReachable]) {
        [Utils showNotReachable:self];
        return;
    }
    
    RoleManager *roleMgr = [RoleManager sharedManager];
    roleMgr.isTrader = !roleMgr.isTrader;
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [roleMgr getAnnouncement:^(NSString *contentStr, BOOL maintenance, BOOL invalidTime, BOOL timeout) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
        // maintenance
//        if (timeout || maintenance) {
//            [Utils showOnMaintenance];
//            
//            // invalid handset time
//        } else if (invalidTime) {
//            Language *lang = [Language sharedInstance];
//            [Utils showAlertWithTitle:[lang get:@"errorInvalidHandsetTime"] message:nil];
//            
//            // has announcement
//        } else if ([contentStr length] > 0) {
//            AnnouncementViewController *announcementCtrl = [Utils getStoryboardViewController:@"AnnouncementViewController"];
//            announcementCtrl.contentStr = contentStr;
//            [self.navigationController pushViewController:announcementCtrl animated:YES];
//            
//            // no announcement
//        } else  if ([contentStr isEqualToString:@""]) {
//            
//            [self.tableView reloadData];
//            
//        } else {
//            Language *lang = [Language sharedInstance];
//            [Utils showAlertWithTitle:[lang get:@"errorNetworkNotReachable"] message:nil];
//        }
        
        [self.tableView reloadData];
        [self.tableView setContentOffset:CGPointMake(0, 0)];
    }];
}

- (void)reloadRootViewController
{
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    
    Language *lang = [Language sharedInstance];
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    NSString *storyboardName = @"Main";
    UIStoryboard *storybaord = [UIStoryboard storyboardWithName:storyboardName bundle:[lang getBundle]];
    delegate.window.rootViewController = [storybaord instantiateInitialViewController];
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
    
}

#pragma mark - Events

- (IBAction)sharePressed
{
    Language *lang = [Language sharedInstance];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[lang get:@"share"]
                                                             delegate:self
                                                    cancelButtonTitle:[lang get:@"cancel"]
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:[lang get:@"shareToFb"], [lang get:@"shareToEmail"], nil];
    [actionSheet showInView:self.view];
}

- (IBAction)languagePressed{
    
    Language *lang = [Language sharedInstance];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[lang get:@"language"]
                                                             delegate:self
                                                    cancelButtonTitle:[lang get:@"cancel"]
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"繁體中文",@"简体中文",@"English", nil];
    [actionSheet showInView:self.view];
    
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    Language *lang = [Language sharedInstance];
    if([actionSheet.title isEqualToString:[lang get:@"language"]]){
        
        if (buttonIndex == 0) {
            [lang setLanguage:@"zh-Hant"];
        }
        
        if (buttonIndex == 2) {
            [lang setLanguage:@"en"];
        }
        
        if (buttonIndex == 1) {
            [lang setLanguage:@"zh-Hans"];
        }
        
        [self reloadRootViewController];
        
        return;
    }
    
    if (buttonIndex == 0) {
        Language *lang = [Language sharedInstance];
        //        PDFBManager *fbMgr = [PDFBManager sharedManager];
        //        [fbMgr shareTitle:[lang get:@"shareTitle"] body:[lang get:@"shareBody"] link:SHARE_URL];
        
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = [NSURL URLWithString:SHARE_URL];
        content.contentTitle = [lang get:@"shareTitle"];
        content.contentDescription = [lang get:@"shareBody"];
        
        FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
        dialog.fromViewController = self;
        dialog.shareContent = content;
        dialog.mode = FBSDKShareDialogModeFeedWeb;
        [dialog show];
        
        
    } else if (buttonIndex == 1) {
        [self processShareToEmail];
    }
}

- (void)processShareToEmail
{
    Language *lang = [Language sharedInstance];
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:[lang get:@"shareTitle"]];
        
        NSString *bodyStr = [NSString stringWithFormat:@"%@: %@", [lang get:@"shareBody"], SHARE_URL];
        [mailer setMessageBody:bodyStr isHTML:NO];
        [self presentViewController:mailer animated:YES completion:nil];
        
    } else {
        [Utils showAlertWithTitle:[lang get:@"errorCannotShareToEmail"] message:nil];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
