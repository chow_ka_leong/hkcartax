//
//  MenuViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"

@interface MenuViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)sharePressed;

- (IBAction)languagePressed;

@end
