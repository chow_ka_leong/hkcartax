//
//  SearchChassisViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "SearchChassisViewController.h"
#import "AutoInfoViewController.h"
#import "SearchManager.h"

@interface SearchChassisViewController () <UIAlertViewDelegate>

@end

@implementation SearchChassisViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    Language *lang = [Language sharedInstance];
    self.titleLbl.text = [lang get:@"searchTitle2"];
    self.textField.placeholder = [lang get:@"searchHintChassis"];
    self.mandatoryLbl.text = [lang get:@"remarkMandatory"];
    [self.searchBtn setTitle:[lang get:@"search"] forState:UIControlStateNormal];
    [self.resetBtn setTitle:[lang get:@"reset"] forState:UIControlStateNormal];
    
    UIImage *blueImage = [[UIImage imageNamed:@"btn_blue.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    [self.searchBtn setBackgroundImage:blueImage forState:UIControlStateNormal];
    
    UIImage *greyImage = [[UIImage imageNamed:@"btn_grey.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    [self.resetBtn setBackgroundImage:greyImage forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Events

- (IBAction)searchPressed
{
    NSString *chassisStr = self.textField.text;
    Language *lang = [Language sharedInstance];
    
    if ([chassisStr length] == 0) {
        [Utils showAlertWithTitle:[lang get:@"errorChassisRequired"] message:nil];
        return;
    }
    
    ChassisQuery *query = [[ChassisQuery alloc] init];
    query.chassisStr = chassisStr;
    
    SearchManager *searchMgr = [SearchManager sharedManager];
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [searchMgr searchByChassis:query saveHistory:YES callback:^(NSArray *resultList, NSString *errorStr) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
        if (errorStr) {
            if ([errorStr isEqualToString:[lang get:@"errorServerNotAvailable"]]) {
                [Utils showOnMaintenance:self serverDown:YES];
            } else if ([errorStr isEqualToString:[lang get:@"errorServerMaintenance"]]) {
                [Utils showOnMaintenance:self serverDown:NO];
            } else {
                [Utils showAlertWithTitle:errorStr message:nil];
            }
        } else if (resultList == nil) {
            Language *lang = [Language sharedInstance];
            [Utils showAlertWithTitle:[lang get:@"noChassisRecord"] message:nil];
        } else {
            AutoDetail *autoDetail = [resultList objectAtIndex:0];
            AutoInfoViewController *autoViewCtrl = [Utils getStoryboardViewController:@"AutoInfoViewController"];
            autoViewCtrl.autoDetail = autoDetail;
            [self.navigationController pushViewController:autoViewCtrl animated:YES];
        }
    }];
    
}

- (IBAction)resetPressed
{
    self.textField.text = nil;
}

- (IBAction)viewTapped
{
    [self.textField resignFirstResponder];
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end
