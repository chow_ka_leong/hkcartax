//
//  HistoryTabViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 27/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "HistoryTabViewController.h"
#import "SearchTabViewController.h"

@interface HistoryTabViewController ()

@end

@implementation HistoryTabViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        Language *lang = [Language sharedInstance];
        self.title = [lang get:@"historyTitle"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"green_gradient.png"]];
    
    self.topTabView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.topTabView.layer.shadowOffset = CGSizeMake(0.0f, 0.5f);
    self.topTabView.layer.shadowOpacity = 0.5f;
    
    Language *lang = [Language sharedInstance];
    [self.buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIButton *btn = (UIButton *)obj;
        NSString *btnStr = [NSString stringWithFormat:@"searchTab%lu", idx + 1];
        [btn setTitle:[lang get:btnStr] forState:UIControlStateNormal];
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    UIButton *btn = [self.buttons objectAtIndex:self.selectedIndex];
    NSString *identifierStr = [NSString stringWithFormat:@"viewController%lu", self.selectedIndex + 1];
    [self performSegueWithIdentifier:identifierStr sender:btn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    NSUInteger index = [self.buttons indexOfObject:sender];
    self.parentViewCtrl.selectedIndex = index;
    self.selectedIndex = index;
}


@end
