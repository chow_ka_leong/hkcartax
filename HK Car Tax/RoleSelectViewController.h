//
//  RoleSelectViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 27/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoleSelectViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *buyerBtn;
@property (weak, nonatomic) IBOutlet UIButton *traderBtn;
@property (weak, nonatomic) IBOutlet UILabel *versionLbl;

- (IBAction)rolePressed:(UIButton *)sender;

@end
