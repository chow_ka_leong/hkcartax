//
//  ModelTableViewCell.m
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "ModelTableViewCell.h"

@implementation ModelTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setQuery:(ModelQuery *)query
{
    _query = query;
    
    Language *lang = [Language sharedInstance];
    self.titleLbl.text = [NSString stringWithFormat:@"%@ - %@", query.make.name, query.model.name];
    self.manufactYearLbl.text = [NSString stringWithFormat:@"%@: %@", [lang get:@"manufactYear"], query.manuYearStr];
    self.modelYearLbl.text = [NSString stringWithFormat:@"%@: %@", [lang get:@"modelYear"], query.modelYearStr];
    self.modelCodeLbl.text = [NSString stringWithFormat:@"%@: %@", [lang get:@"modelCode"], query.modelCodeStr];
}

@end
