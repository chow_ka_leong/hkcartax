//
//  PRPTableViewCell.m
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "PRPTableViewCell.h"

@implementation PRPTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setQuery:(PRPQuery *)query
{
    _query = query;
    
    Language *lang = [Language sharedInstance];
    self.titleLbl.text = [NSString stringWithFormat:@"P%@/%@", query.prpStr ,query.prpYearStr];
    self.makeLbl.text = query.make.name;
    self.effDateLbl.text = [NSString stringWithFormat:@"%@: %@", [lang get:@"effDate"], [query getEffDateStr]];
}


@end
