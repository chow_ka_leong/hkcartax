//
//  SplashScreenViewController.m
//  HK Car Tax
//
//  Created by louis on 25/4/2016.
//  Copyright © 2016 Rocky Wong. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "APIManager.h"
#import "Harpy.h"
#import "DisclaimerViewController.h"
#import "GDNavigationController.h"

@interface SplashScreenViewController () <UIAlertViewDelegate, HarpyDelegate>

@property (nonatomic, assign) BOOL startUpChecked;
@property (nonatomic, assign) BOOL hasVersionUpdate;

@end

@implementation SplashScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.startUpChecked) {
        
        [self performSegueWithIdentifier:@"goToRoleSelectionPage" sender:nil];
        return;
    }
    
    if (![Utils isServerReachable]) {
        [Utils showNotReachable:self];
        return;
    }
    
    APIManager *apiMgr = [APIManager sharedManager];
    
    [apiMgr getAccessTokenOnStart:^(NSInteger statusCode, NSString *errStr) {
        
        Language *lang = [Language sharedInstance];
        if (errStr) {
            if ([errStr isEqualToString:[lang get:@"errorServerNotAvailable"]]) {
                [Utils showOnMaintenance:self serverDown:YES];
            } else if ([errStr isEqualToString:[lang get:@"errorServerMaintenance"]]) {
                [Utils showOnMaintenance:self serverDown:NO];
            } else {
                [Utils showAlertWithTitle:errStr message:nil];
            }
            
        } else {
            [[Harpy sharedInstance] setDelegate:self];
            [[Harpy sharedInstance] checkVersion];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (!self.hasVersionUpdate) {
                    [self openDisclaimer];
                }
            });
        }
    
    }];
}

-(void)openDisclaimer {
    
    self.startUpChecked = YES;
    RoleManager *roleMgr = [RoleManager sharedManager];
    if (![roleMgr isDisclaimerAccepted]) {
        DisclaimerViewController *disclaimerCtrl = [Utils getStoryboardViewController:@"DisclaimerViewController"];
        GDNavigationController *navCtrl = [[GDNavigationController alloc] initWithRootViewController:disclaimerCtrl];
        [self presentViewController:navCtrl animated:NO completion:nil];
    } else {
        [self performSegueWithIdentifier:@"goToRoleSelectionPage" sender:nil];
    }
    
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    BOOL forceUpdate = [[Harpy sharedInstance] isForceUpdateVersion];
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    } else {
        
        if (buttonIndex == 1) {
            [self launchAppStore];
            if (!forceUpdate) {
                [self openDisclaimer];
            }
        } else {
            [self openDisclaimer];
        }
    }
}

- (void)harpyDidDetectNewVersionWithoutAlert:(NSString *)message {
    
    self.hasVersionUpdate = YES;
    
    Language *lang = [Language sharedInstance];
    
    BOOL forceUpdate = [[Harpy sharedInstance] isForceUpdateVersion];
    
    NSString *title = [lang get:@"appUpdateMessage"];
    
    if (NSStringFromClass([UIAlertController class])) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:[lang get:@"update"]
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action){
                                                              [self launchAppStore];
                                                              if (!forceUpdate) {
                                                                  [self openDisclaimer];
                                                              }
                                                          }]];
        
//        if (!forceUpdate) {
//            [alertController addAction:[UIAlertAction actionWithTitle:[lang get:@"cancel"]
//                                                                style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction *action){
//                                                                  [self openDisclaimer];
//                                                              }]];
//        }
        
        alertController.view.frame = [[UIScreen mainScreen] applicationFrame];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        
        UIAlertView *alert;
        
        alert = [[UIAlertView alloc] initWithTitle:title
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:forceUpdate ? nil:[lang get:@"cancel"]
                                 otherButtonTitles:[lang get:@"update"], nil];
        
        [alert show];
    }
}


- (void)launchAppStore
{
    NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", APP_STORE_ID];
    NSURL *iTunesURL = [NSURL URLWithString:iTunesString];
    [[UIApplication sharedApplication] openURL:iTunesURL];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
