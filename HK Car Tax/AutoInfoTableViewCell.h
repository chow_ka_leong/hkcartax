//
//  AutoInfoTableViewCell.h
//  HK Car Tax
//
//  Created by Rocky Wong on 28/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *modelCodeLbl;
@property (weak, nonatomic) IBOutlet UILabel *manfactYearLbl;
@property (weak, nonatomic) IBOutlet UILabel *effDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *a1PriceLbl;
@property (strong, nonatomic) AutoDetail *autoDetail;

- (void)refresh;

@end
