//
//  GDNavigationController.m
//  DigitalAlbumV3
//
//  Created by Rocky Wong on 28/11/13.
//
//

#import "GDNavigationController.h"

@interface GDNavigationController ()

@end

@implementation GDNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([Utils iOS7orAbove])
        self.navigationBar.tintColor = [UIColor darkGrayColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // You do not need this method if you are not supporting earlier iOS Versions
    return [self.topViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [super pushViewController:viewController animated:animated];
    
    Language *lang = [Language sharedInstance];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:[lang get:@"back"]
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self
                                                                action:@selector(backPressed)];
    self.navigationBar.topItem.backBarButtonItem = backItem;
}

- (IBAction)backPressed
{
    [self popViewControllerAnimated:YES];
}


@end
