//
//  GDTabBarController.h
//  DigitalAlbumV3
//
//  Created by Rocky Wong on 28/11/13.
//
//

#import <UIKit/UIKit.h>

@interface GDTabBarController : UITabBarController

@end
