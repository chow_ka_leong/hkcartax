//
//  Language.h
//  DigitalAlbumV3
//
//  Created by Rocky Wong on 5/12/13.
//
//

#import <Foundation/Foundation.h>

@interface Language : NSObject

+ (id)sharedInstance;

- (NSString *)get:(NSString *)key;
- (NSString *)currentLanguage;
-(void)setLanguage:(NSString *)l;
- (BOOL)en;
- (BOOL)tc;
- (BOOL)sc;

- (void)addLangObserver:(id)observer selector:(SEL)selector;
- (void)removeLangObserver:(id)observer;
- (void)postLangChangedNotification;

- (NSBundle*) getBundle;

@end
