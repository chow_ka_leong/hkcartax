//
//  BundleEx.h
//  HK Car Tax
//
//  Created by Leong on 26/6/2018.
//  Copyright © 2018 Rocky Wong. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+ (void)setLanguage:(NSString *)language;

@end
