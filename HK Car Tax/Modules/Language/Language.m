//
//  Language.m
//  DigitalAlbumV3
//
//  Created by Rocky Wong on 5/12/13.
//
//

#import "Language.h"

#define kLanguageKey @"LanguageKey"
#define kLanguageChangedNotification @"language_changed_notification"

@interface Language ()

@property (strong, nonatomic) NSBundle *bundle;

@end

@implementation Language

#pragma mark Singleton Methods

+ (id)sharedInstance
{
    static Language *shareInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[self alloc] init];
    });
    return shareInstance;
}

- (id)init
{
    if (self = [super init]) {
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

- (NSBundle*) getBundle{
    return self.bundle;
}

- (NSString *)get:(NSString *)key
{
    return NSLocalizedString(key, nil);
}

- (NSString *)currentLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    
    return [languages objectAtIndex:0];
}

-(void)setLanguage:(NSString *)l {
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:l, nil]
                                              forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [NSBundle setLanguage:l];
    
}

- (BOOL)en { return [[self currentLanguage] isEqualToString:@"en"];}
- (BOOL)tc { return ([[self currentLanguage] rangeOfString:@"zh-Hant"].location != NSNotFound || [[self currentLanguage] rangeOfString:@"zh-HK"].location != NSNotFound || [[self currentLanguage] rangeOfString:@"zh-TW"].location != NSNotFound); }
- (BOOL)sc { return [[self currentLanguage] rangeOfString:@"zh-Hans"].location != NSNotFound ;}


#pragma mark - Notification

- (void)addLangObserver:(id)observer selector:(SEL)selector
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:observer selector:selector name:kLanguageChangedNotification object:self];
}

- (void)removeLangObserver:(id)observer
{
    [[NSNotificationCenter defaultCenter] removeObserver:observer name:kLanguageChangedNotification object:self];
}

- (void)postLangChangedNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kLanguageChangedNotification object:self userInfo:nil];
}

@end
