//
//  GDButton.m
//  HK Car Tax
//
//  Created by Rocky Wong on 4/4/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "GDButton.h"

@implementation GDButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if (selected)
        self.backgroundColor = TAB_GREEN;
    else
        self.backgroundColor = [UIColor lightGrayColor];
}

@end
