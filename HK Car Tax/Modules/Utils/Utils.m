//
//  Utils.m
//  DigitalAlbumV3
//
//  Created by Rocky on 14/11/13.
//
//

#import <sys/utsname.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "Utils.h"
#import "NSString+SSToolkitAdditions.h"
#import "Reachability.h"

@implementation Utils

#pragma mark - Device & System

+ (NSString *)appID
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
}

+ (NSString *)appVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

+ (NSString *)appName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}

+ (NSString *)deviceModel
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];

    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 2G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4 (CDMA)";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch (1 Gen)";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch (2 Gen)";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch (3 Gen)";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch (4 Gen)";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch (5 Gen)";
    
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad1,2"])      return @"iPad 3G";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2 (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2 (Cellular)";
    
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    return platform;
}

+ (NSString *)deviceIdentifier
{
//    return [BPXLUUIDHandler UUID];
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
//    CFUUIDRef theUUID = CFUUIDCreate(NULL);
//    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
//    CFRelease(theUUID);
//    TRACE(@"string: %@", string);
//    return (__bridge NSString *)string;
}

+ (NSString *)systemVersion
{
   return [[UIDevice currentDevice] systemVersion];
}

+ (BOOL)isIPad
{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

+ (BOOL)isIPhone5
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (screenSize.height > 480.0f) {
            return YES;
        } else {
            return NO;
        }
    }
    
    return NO;
}

+ (BOOL)iOS7orAbove
{
    NSString *currentVerStr = [[UIDevice currentDevice] systemVersion];
    NSString *minVersionStr = @"7.0";
    //currentVerStr = @"7.0";
    NSComparisonResult result = [minVersionStr compareToVersionString:currentVerStr];
    return result != NSOrderedDescending;
}

+ (NSString *)IPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

#pragma mark - String

+ (NSDate *)dateWithFormat:(NSString *)dateFormat string:(NSString *)string
{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:enUSPOSIXLocale];
    }
    [dateFormatter setDateFormat:dateFormat];

    return [dateFormatter dateFromString:string];
}

+ (NSString *)dateStringWithDateFormat:(NSString *)dateFormat date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:enUSPOSIXLocale];
    }
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter stringFromDate:date];
}

+ (BOOL)isValidEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

+ (BOOL)isValidPassword:(NSString *)pwdStr
{
    NSString *pwdRegex = @"^(?=[a-zA-Z\\d]{8,20}$)(?=.*?[a-zA-Z])(?=.*?\\d).*";
    NSPredicate *pwdTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pwdRegex];
    return [pwdTest evaluateWithObject:pwdStr];
}

+ (id)getObjectOrNil:(id)obj
{
    if (obj == [NSNull null])
        return nil;
    else
        return obj;
}

+ (NSString *)getStrOrNil:(id)obj
{
    return [[self getObjectOrNil:obj] stringValue];
}

+ (NSURL *)getURLOrNil:(id)obj
{
    NSString *urlStr = [Utils getObjectOrNil:obj];
    if (urlStr)
        return [NSURL URLWithString:urlStr];
    else
        return nil;
}

#pragma mark - UI

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message quitOnConfirm:(BOOL)quitOnConfirm delegate:(id<UIAlertViewDelegate>)delegate
{
    Language *lang = [Language sharedInstance];
    
    NSString *buttonText = [lang get:quitOnConfirm ? @"quit":@"confirm"];
    
    if (NSStringFromClass([UIAlertController class])) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:buttonText
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action){
                                                              
                                                              if (quitOnConfirm) {
                                                                  exit(0);
                                                              }
                                                          }]];
        
        alertController.view.frame = [[UIScreen mainScreen] applicationFrame];
        [[UIApplication sharedApplication].topViewController presentViewController:alertController animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:buttonText
                                              otherButtonTitles:nil];
        alert.delegate = delegate;
        alert.tag = ALERT_WILL_QUIT_TAG;
        [alert show];
    }
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    [Utils showAlertWithTitle:title message:message quitOnConfirm:NO delegate:nil];
}

+ (UILabel *)getNavigationTitleView:(NSString *)titleStr
{
//    FXLabel *titleLbl = [[FXLabel alloc] init];
    UILabel *titleLbl = [[UILabel alloc] init];
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.font = [UIFont systemFontOfSize:20];
    titleLbl.text = titleStr;
    titleLbl.textColor = [UIColor whiteColor];
//    UIColor *c1 = [UIColor colorWithRed:161/255.0 green:159/255.0 blue:164/255.0 alpha:1];
//    UIColor *c2 = [UIColor whiteColor];
//    titleLbl.gradientColors = @[c1, c2, c1];
//    titleLbl.gradientStartPoint = CGPointMake(0.0f, 0.5f);
//    titleLbl.gradientEndPoint = CGPointMake(1.0f, 0.5f);
//    titleLbl.oversampling = 2;
    [titleLbl sizeToFit];
    return titleLbl;
}

+ (UITableViewCell *)getReloadingCell:(UITableView *)tableView
{
    static NSString *cellIdendifier = @"ReloadingCellIdendifier";
    static int const ACTIVITY_VIEW_TAG = 30561;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdendifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdendifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityView.tag = ACTIVITY_VIEW_TAG;
        activityView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        activityView.center = cell.contentView.center;
        [cell.contentView addSubview:activityView];
    }
    
    UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)[cell.contentView viewWithTag:ACTIVITY_VIEW_TAG];
    [activityView startAnimating];
    
    return cell;
}

+ (UILabel *)getNoContentLabel
{
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:12];
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    Language *lang = [Language sharedInstance];
    label.text = [lang get:@"noRecord"];
    [label sizeToFit];
    return label;
}

+ (UIViewController *)getTopViewController:(UIViewController *)viewCtrl
{
    UIViewController *topViewCtrl = viewCtrl;
    while (topViewCtrl.presentedViewController) {
        topViewCtrl = viewCtrl.presentedViewController;
    }
    return topViewCtrl;
}

+ (id)getStoryboardViewController:(NSString *)identifier
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:identifier];
}

#pragma mark - Reachability

+ (BOOL)isServerReachable
{
//    Reachability *reachability = [Reachability reachabilityWithHostName:SERVER_HOST_NAME];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    [reachability startNotifier];
//    TRACE(@"reach: %@", reachability);
    
    NetworkStatus status = [reachability currentReachabilityStatus];
//    TRACE(@"status: %d", status);
    if (status == NotReachable)
        return NO;
    else
        return YES;
}

+ (void)showNotReachable:(id<UIAlertViewDelegate>)delegate
{
    Language *lang = [Language sharedInstance];
    [self showAlertWithTitle:[lang get:@"errorNetworkNotReachable"] message:nil quitOnConfirm:YES delegate:delegate];
}

+ (BOOL)onMaintenance:(NSHTTPURLResponse *)response
{
    if (response && response.statusCode == 0) {
        return YES;
    }
    NSDictionary *header = [response allHeaderFields];
    return [header objectForKey:@"Is-SCG-Maintenance"] != nil;
}

+ (BOOL)onServerTimeout:(NSHTTPURLResponse *)response
{
    return (!response || response.statusCode == 0);
}

+ (void)showOnMaintenance:(id<UIAlertViewDelegate>)delegate serverDown:(BOOL)serverDown
{
    Language *lang = [Language sharedInstance];
    NSString *title = serverDown ? @"errorServerNotAvailable":@"errorServerMaintenance";
    
    if (serverDown && ![Utils isServerReachable]) {
        [self showNotReachable:delegate];
    } else {
        [self showAlertWithTitle:[lang get:title] message:nil quitOnConfirm:YES delegate:delegate];
    }
}

+ (BOOL)isViewPotrait
{
    if (([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft) || ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft)){
        return NO;
    }
    return YES;
}

@end
