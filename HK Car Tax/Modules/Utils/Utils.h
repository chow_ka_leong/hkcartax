//
//  Utils.h
//  DigitalAlbumV3
//
//  Created by Rocky on 14/11/13.
//
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

#pragma mark - Device & System

+ (NSString *)appID;
+ (NSString *)appVersion;
+ (NSString *)appName;
+ (NSString *)deviceModel;
+ (NSString *)deviceIdentifier;
+ (NSString *)systemVersion;
+ (NSString *)IPAddress;
+ (BOOL)isIPad;
+ (BOOL)isIPhone5;
+ (BOOL)iOS7orAbove;

#pragma mark - String

+ (NSDate *)dateWithFormat:(NSString *)dateFormat string:(NSString *)string;
+ (NSString *)dateStringWithDateFormat:(NSString *)dateFormat date:(NSDate *)date;

+ (BOOL)isValidEmail:(NSString *)emailStr;
+ (BOOL)isValidPassword:(NSString *)pwdStr;
+ (id)getObjectOrNil:(id)obj;
+ (NSString *)getStrOrNil:(id)obj;
+ (NSURL *)getURLOrNil:(id)obj;

#pragma mark - UI

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
+ (UILabel *)getNavigationTitleView:(NSString *)titleStr;
+ (UITableViewCell *)getReloadingCell:(UITableView *)tableView;
+ (UILabel *)getNoContentLabel;
+ (UIViewController *)getTopViewController:(UIViewController *)viewCtrl;
+ (id)getStoryboardViewController:(NSString *)identifier;

#pragma mark - Reachabiliy

+ (BOOL)isServerReachable;
+ (void)showNotReachable:(id<UIAlertViewDelegate>)delegate;
+ (BOOL)onMaintenance:(NSHTTPURLResponse *)response;
+ (void)showOnMaintenance:(id<UIAlertViewDelegate>)delegate serverDown:(BOOL)serverDown;
+ (BOOL)onServerTimeout:(NSHTTPURLResponse *)response;

#pragma mark - Helper

+ (BOOL)isViewPotrait;

@end
