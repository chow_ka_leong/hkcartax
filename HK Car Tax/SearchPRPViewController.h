//
//  SearchPRPViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPAutoCompleteTextField.h"

@interface SearchPRPViewController : UIViewController <UITextFieldDelegate, MLPAutoCompleteTextFieldDelegate, MLPAutoCompleteTextFieldDataSource, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *makeTextField;
@property (weak, nonatomic) IBOutlet UILabel *prpLbl;
@property (weak, nonatomic) IBOutlet UITextField *prpTextField;
@property (weak, nonatomic) IBOutlet UITextField *prpYearTextField;
@property (weak, nonatomic) IBOutlet UITextField *effDateTextField;
@property (weak, nonatomic) IBOutlet UILabel *mandatoryLbl;

@end
