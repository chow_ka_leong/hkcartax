//
//  DisclaimerViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 28/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "DisclaimerViewController.h"

@interface DisclaimerViewController ()

@end

@implementation DisclaimerViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        Language *lang = [Language sharedInstance];
        self.title = [lang get:@"disclaimer"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *blueImage = [[UIImage imageNamed:@"btn_blue.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    [self.acceptBtn setBackgroundImage:blueImage forState:UIControlStateNormal];
    
    UIImage *greyImage = [[UIImage imageNamed:@"btn_grey.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    [self.declineBtn setBackgroundImage:greyImage forState:UIControlStateNormal];
    
    Language *lang = [Language sharedInstance];
    self.textView.text = [lang get:@"disclaimerContent"];
    [self.acceptBtn setTitle:[lang get:@"accept"] forState:UIControlStateNormal];
    [self.declineBtn setTitle:[lang get:@"decline"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)acceptPressed
{
    RoleManager *roleMgr = [RoleManager sharedManager];
    [roleMgr acceptDisclaimer];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)declinePressed
{
    exit(0);
}

@end
