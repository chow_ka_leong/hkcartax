//
//  AutoInfoViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoInfoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *calculatorButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) AutoDetail *autoDetail;

@end
