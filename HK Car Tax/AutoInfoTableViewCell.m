//
//  AutoInfoTableViewCell.m
//  HK Car Tax
//
//  Created by Rocky Wong on 28/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "AutoInfoTableViewCell.h"

@implementation AutoInfoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)refresh
{
    Language *lang = [Language sharedInstance];
    self.titleLbl.text = [NSString stringWithFormat:@"%@ - %@", self.autoDetail.makeStr, self.autoDetail.modelNameStr];
    self.modelCodeLbl.text = self.autoDetail.modelCodeStr;
    self.manfactYearLbl.text = [NSString stringWithFormat:@"%@ %@", [lang get:@"manufactYear"], self.autoDetail.manufactYearStr];
    self.effDateLbl.text = [NSString stringWithFormat:@"%@ %@", [lang get:@"effDate"], self.autoDetail.effDateStr];
    self.a1PriceLbl.text = [NSString stringWithFormat:@"%@ $%@", [lang get:@"a1Accessory"], self.autoDetail.a1PriceStr];
}

@end
