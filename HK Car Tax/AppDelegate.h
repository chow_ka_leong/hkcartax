//
//  AppDelegate.h
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Harpy.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, HarpyDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
