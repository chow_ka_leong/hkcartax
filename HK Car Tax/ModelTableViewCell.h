//
//  ModelTableViewCell.h
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *manufactYearLbl;
@property (weak, nonatomic) IBOutlet UILabel *modelYearLbl;
@property (weak, nonatomic) IBOutlet UILabel *modelCodeLbl;
@property (strong, nonatomic) ModelQuery *query;

@end
