//
//  RequestHeader.m
//  HK Car Tax
//
//  Created by Rocky Wong on 20/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "RequestHeader.h"

@implementation RequestHeader

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (NSMutableDictionary *)getPostJSON
{
    NSMutableDictionary *header = [self getHeaderJSON];
    return [NSMutableDictionary dictionaryWithObject:header forKey:@"requestHeader"];
}

- (NSMutableDictionary *)getHeaderJSON
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[Utils IPAddress] forKey:@"ip"];
    [dict setObject:@"iOS" forKey:@"osPlatform"];
    [dict setObject:[Utils systemVersion] forKey:@"osVersion"];
    [dict setObject:[Utils appVersion] forKey:@"appVersion"];
    [dict setObject:[Utils deviceIdentifier] forKey:@"deviceId"];
    
    int timestamp = (int)[[NSDate date] timeIntervalSince1970];
    [dict setObject:@(timestamp) forKey:@"requestId"];
    
    if (self.version > 0)
        [dict setObject:@(self.version) forKey:@"version"];
    
    return dict;
}

@end
