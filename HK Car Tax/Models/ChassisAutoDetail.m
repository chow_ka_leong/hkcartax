//
//  ChassisAutoDetail.m
//  HK Car Tax
//
//  Created by Rocky Wong on 4/4/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "ChassisAutoDetail.h"

@implementation ChassisAutoDetail

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super initWithDict:dict];
    if (self) {
        AutoSection *firstSec = [self.sectionList objectAtIndex:0];
        [firstSec.afList addObject:[self getField:@"chasNum"]];
    }
    return self;
}

@end
