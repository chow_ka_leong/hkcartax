//
//  SearchQuery.h
//  HK Car Tax
//
//  Created by Rocky Wong on 20/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchQuery : NSObject <NSCoding>

- (NSMutableDictionary *)getQueryParams;

@end

@interface ModelQuery : SearchQuery

@property (strong, nonatomic) Make *make;
@property (strong, nonatomic) Model *model;
@property (copy, nonatomic) NSString *manuYearStr;
@property (copy, nonatomic) NSString *modelYearStr;
@property (copy, nonatomic) NSString *modelCodeStr;

@end

@interface ChassisQuery : SearchQuery

@property (copy, nonatomic) NSString *chassisStr;

@end

@interface PRPQuery : SearchQuery

@property (strong, nonatomic) Make *make;
@property (copy, nonatomic) NSString *prpStr;
@property (copy, nonatomic) NSString *prpYearStr;
@property (copy, nonatomic) NSDate *effDate;

- (NSString *)getEffDateStr;

@end
