//
//  Make.m
//  HK Car Tax
//
//  Created by Rocky Wong on 19/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "Make.h"

@implementation Make

static NSString *const MAKE_NAME_KEY = @"MAKE_NAME_KEY";
static NSString *const MAKE_CODE_KEY = @"MAKE_CODE_KEY";

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.code = [dict objectForKey:@"makeCode"];
        self.name = [dict objectForKey:@"makeDes"];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.code = [aDecoder decodeObjectForKey:MAKE_CODE_KEY];
        self.name = [aDecoder decodeObjectForKey:MAKE_NAME_KEY];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.code forKey:MAKE_CODE_KEY];
    [aCoder encodeObject:self.name forKey:MAKE_NAME_KEY];
}

- (BOOL)isEqual:(id)object
{
    Make *m = (Make *)object;
    return [self.code isEqualToString:m.code];
}

- (void)setModels:(NSArray *)modelInfoList
{
    self.modelList = [NSMutableArray array];
    
    [modelInfoList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Model *model = [[Model alloc] initWithDict:obj];
        [self.modelList addObject:model];
    }];
}

- (NSString *)description
{
    return self.code;
}



@end
