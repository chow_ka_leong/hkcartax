//
//  AutoDetail.h
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AutoField : NSObject

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *value;

@end

@interface AutoSection : NSObject

@property (copy, nonatomic) NSString *name;
@property (strong, nonatomic) NSMutableArray *afList;

@end

@interface AutoDetail : NSObject

@property (copy, nonatomic) NSString *makeStr;
@property (copy, nonatomic) NSString *modelNameStr;
@property (copy, nonatomic) NSString *modelCodeStr;
@property (copy, nonatomic) NSString *manufactYearStr;
@property (copy, nonatomic) NSString *effDateStr;
@property (copy, nonatomic) NSString *a1PriceStr;
@property (strong, nonatomic) NSDictionary *autoInfo;
@property (strong, nonatomic) NSMutableArray *sectionList;

- (id)initWithDict:(NSDictionary *)dict;
- (AutoField *)getFieldAtIndexPath:(NSIndexPath *)indexPath;

// Utils
- (AutoField *)getField:(NSString *)nameStr;

@end
