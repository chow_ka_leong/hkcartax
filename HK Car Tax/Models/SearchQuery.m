//
//  SearchQuery.m
//  HK Car Tax
//
//  Created by Rocky Wong on 20/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "SearchQuery.h"
#import "RequestHeader.h"

@implementation SearchQuery

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
}

- (NSMutableDictionary *)getQueryParams
{
    RequestHeader *header = [[RequestHeader alloc] init];
    NSMutableDictionary *params = [header getPostJSON];

    Language *lang = [Language sharedInstance];
    
    if ([lang tc])
        [params setObject:@"TC" forKey:@"lang"];
    else if ([lang sc])
        [params setObject:@"SC" forKey:@"lang"];
    else
        [params setObject:@"EN" forKey:@"lang"];
    
    return params;
}

@end

@implementation ModelQuery

static NSString *const MODEL_MAKE_KEY = @"MODEL_MAKE_KEY";
static NSString *const MODEL_MODEL_KEY = @"MODEL_MODEL_KEY";
static NSString *const MODEL_MANU_YEAR_KEY = @"MODEL_MANU_YEAR_KEY";
static NSString *const MODEL_MODEL_YEAR_KEY = @"MODEL_MODEL_YEAR_KEY";
static NSString *const MODEL_MODEL_CODE_KEY = @"MODEL_MODEL_CODE_KEY";

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.make = [aDecoder decodeObjectForKey:MODEL_MAKE_KEY];
        self.model = [aDecoder decodeObjectForKey:MODEL_MODEL_KEY];
        self.manuYearStr = [aDecoder decodeObjectForKey:MODEL_MANU_YEAR_KEY];
        self.modelYearStr = [aDecoder decodeObjectForKey:MODEL_MODEL_YEAR_KEY];
        self.modelCodeStr = [aDecoder decodeObjectForKey:MODEL_MODEL_CODE_KEY];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.make forKey:MODEL_MAKE_KEY];
    [aCoder encodeObject:self.model forKey:MODEL_MODEL_KEY];
    [aCoder encodeObject:self.manuYearStr forKey:MODEL_MANU_YEAR_KEY];
    [aCoder encodeObject:self.modelYearStr forKey:MODEL_MODEL_YEAR_KEY];
    [aCoder encodeObject:self.modelCodeStr forKey:MODEL_MODEL_CODE_KEY];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %@, %@, %@, %@", self.make, self.model, self.manuYearStr, self.modelYearStr, self.modelCodeStr];
}

- (NSMutableDictionary *)getQueryParams
{
    NSMutableDictionary *params = [super getQueryParams];
    [params setObject:@(1) forKey:@"searchType"];
    
    NSMutableDictionary *searchDict = [NSMutableDictionary dictionary];
    [params setObject:searchDict forKey:@"searchByModel"];
    
    [searchDict setObject:self.make.code forKey:@"makeCode"];
    [searchDict setObject:self.model.name forKey:@"modelName"];
    [searchDict setObject:self.manuYearStr forKey:@"manufactYear"];
    if ([self.modelYearStr length] > 0)
        [searchDict setObject:self.modelYearStr forKey:@"modelYear"];
    if ([self.modelCodeStr length] > 0)
        [searchDict setObject:self.modelCodeStr forKey:@"modelCode"];
    
    return params;
}

@end

@implementation ChassisQuery

static NSString *const CHASSIS_CHASSIS_KEY = @"CHASSIS_CHASSIS_KEY";

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.chassisStr = [aDecoder decodeObjectForKey:CHASSIS_CHASSIS_KEY];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.chassisStr forKey:CHASSIS_CHASSIS_KEY];
}

- (NSString *)description
{
    return self.chassisStr;
}

- (NSMutableDictionary *)getQueryParams
{
    NSMutableDictionary *params = [super getQueryParams];
    [params setObject:@(2) forKey:@"searchType"];
    
    NSMutableDictionary *searchDict = [NSMutableDictionary dictionary];
    [params setObject:searchDict forKey:@"searchByChassis"];

    [searchDict setObject:self.chassisStr forKey:@"chassis"];
    
    return params;
}

@end

@implementation PRPQuery

static NSString *const PRP_PRP_KEY = @"PRP_PRP_KEY";
static NSString *const PRP_PRP_YEAR_KEY = @"PRP_PRP_YEAR_KEY";
static NSString *const PRP_MAKE_KEY = @"PRP_MAKE_KEY";
static NSString *const PRP_EFF_DATE_KEY = @"PRP_EFF_DATE_KEY";

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.prpStr = [aDecoder decodeObjectForKey:PRP_PRP_KEY];
        self.prpYearStr = [aDecoder decodeObjectForKey:PRP_PRP_YEAR_KEY];
        self.make = [aDecoder decodeObjectForKey:PRP_MAKE_KEY];
        self.effDate = [aDecoder decodeObjectForKey:PRP_EFF_DATE_KEY];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.prpStr forKey:PRP_PRP_KEY];
    [aCoder encodeObject:self.prpYearStr forKey:PRP_PRP_YEAR_KEY];
    [aCoder encodeObject:self.make forKey:PRP_MAKE_KEY];
    [aCoder encodeObject:self.effDate forKey:PRP_EFF_DATE_KEY];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %@, %@, %@", self.make, self.prpStr, self.prpYearStr, self.effDate];
}

- (NSMutableDictionary *)getQueryParams
{
    NSMutableDictionary *params = [super getQueryParams];
    [params setObject:@(3) forKey:@"searchType"];
    
    NSMutableDictionary *searchDict = [NSMutableDictionary dictionary];
    [params setObject:searchDict forKey:@"searchByPrpNum"];
    
    [searchDict setObject:self.prpStr forKey:@"prpNum"];
    [searchDict setObject:self.prpYearStr forKey:@"prpYear"];
    [searchDict setObject:self.make.code forKey:@"makeCode"];
    [searchDict setObject:[self getEffDateStr] forKey:@"effDate"];
    
    return params;
}

- (NSString *)getEffDateStr
{
    return [Utils dateStringWithDateFormat:@"dd/MM/yyyy" date:self.effDate];
}

@end