//
//  Model.h
//  HK Car Tax
//
//  Created by Rocky Wong on 19/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject <NSCoding>

@property (copy, nonatomic) NSString *name;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end
