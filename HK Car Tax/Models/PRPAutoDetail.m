//
//  PRPAutoDetail.m
//  HK Car Tax
//
//  Created by Rocky Wong on 4/4/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "PRPAutoDetail.h"

@implementation PRPAutoDetail

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super initWithDict:dict];
    if (self) {
        AutoSection *firstSec = [self.sectionList objectAtIndex:0];
        [firstSec.afList addObject:[self getField:@"status"]];
    }
    return self;
}

@end
