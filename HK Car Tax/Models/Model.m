//
//  Model.m
//  HK Car Tax
//
//  Created by Rocky Wong on 19/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "Model.h"

@implementation Model

static NSString *const MODEL_NAME_KEY = @"MODEL_NAME_KEY";

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.name = [dict objectForKey:@"modelName"];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.name = [aDecoder decodeObjectForKey:MODEL_NAME_KEY];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:MODEL_NAME_KEY];
}

- (NSString *)description
{
    return self.name;
}

@end
