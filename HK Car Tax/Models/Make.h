//
//  Make.h
//  HK Car Tax
//
//  Created by Rocky Wong on 19/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Make : NSObject <NSCoding>

@property (copy, nonatomic) NSString *code;
@property (copy, nonatomic) NSString *name;
@property (strong, nonatomic) NSMutableArray *modelList;

- (instancetype)initWithDict:(NSDictionary *)dict;
- (void)setModels:(NSArray *)modelInfoList;

@end
