//
//  AutoDetail.m
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "AutoDetail.h"

@implementation AutoField

@end

@implementation AutoSection

@end

@implementation AutoDetail

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.autoInfo = dict;
        self.makeStr = [Utils getStrOrNil:[dict objectForKey:@"makeDesc"]];
        self.modelNameStr = [Utils getStrOrNil:[dict objectForKey:@"modelName"]];
        self.modelCodeStr = [Utils getStrOrNil:[dict objectForKey:@"modelCode"]];
        self.manufactYearStr = [Utils getStrOrNil:[dict objectForKey:@"manufactYear"]];
        self.effDateStr = [Utils getStrOrNil:[dict objectForKey:@"effDate"]];
        self.a1PriceStr = [Utils getStrOrNil:[dict objectForKey:@"a1Price"]];
        
        // AutoDetail in UITableView
        self.sectionList = [NSMutableArray array];
        
        AutoSection *basicSection = [[AutoSection alloc] init];
        [self.sectionList addObject:basicSection];
        NSMutableArray *basicList = [NSMutableArray array];
        basicSection.afList = basicList;
        
        AutoField *prpField = [self getField:@"prpNum"];
        NSString *prpYear = [Utils getStrOrNil:[self.autoInfo objectForKey:@"prpYear"]];
        prpYear = [prpYear substringFromIndex:2];
        prpField.value = [NSString stringWithFormat:@"P%@/%@", prpField.value, prpYear];
        [basicList addObject:prpField];
        
        [basicList addObject:[self getField:@"effDate"]];
        [basicList addObject:[self getField:@"makeDesc"]];
        [basicList addObject:[self getField:@"modelName"]];
        [basicList addObject:[self getField:@"modelCode"]];
        [basicList addObject:[self getField:@"manufactYear"]];
        [basicList addObject:[self getField:@"modelYear"]];

        // engine size
        AutoField *engineField = [self getField:@"engineSize"];
        engineField.value = [engineField.value stringByAppendingString:@"cc"];
        [basicList addObject:engineField];
        
        // seat Num
        AutoField *seatField = [self getField:@"seatNum"];
        [basicList addObject:seatField];
        if ([seatField.value intValue] == 0)
            seatField.value = @" ";
        
        Language *lang = [Language sharedInstance];
        AutoSection *a1Section = [self addOptionSection:@"a1Accessory"];
        AutoField *a1SubTotal = [self getSubTotalField:@"a1Price" title:[lang get:@"a1SubTotal"]];
        [a1Section.afList insertObject:a1SubTotal atIndex:0];
        
        AutoSection *a2Section = [self addOptionSection:@"a2Option"];
        AutoField *a2SubTotal = [self getSubTotalField:@"a2Price" title:[lang get:@"a2SubTotal"]];
        [a2Section.afList insertObject:a2SubTotal atIndex:0];
        
        AutoSection *bSection = [self addOptionSection:@"bOption"];
        AutoField *bSubTotal = [self getSubTotalField:@"bPrice" title:[lang get:@"bSubTotal"]];
        [bSection.afList insertObject:bSubTotal atIndex:0];
        
        AutoSection *cSection = [self addOptionSection:@"cOption"];
        AutoField *cSubTotal = [self getSubTotalField:@"cPrice" title:[lang get:@"cSubTotal"]];
        [cSection.afList insertObject:cSubTotal atIndex:0];
        
    }
    return self;
}

- (AutoField *)getField:(NSString *)nameStr
{
    AutoField *af = [[AutoField alloc] init];
    af.name = nameStr;
    af.value = [Utils getStrOrNil:[self.autoInfo objectForKey:nameStr]];
    return af;
}

- (NSMutableArray *)getAccDesList:(NSArray *)infoList
{
    NSMutableArray *accDesList = [NSMutableArray array];
    [infoList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *dict = (NSDictionary *)obj;
        AutoField *af = [[AutoField alloc] init];
        [accDesList addObject:af];
        
        NSString *accSeq = [Utils getObjectOrNil:[dict objectForKey:@"accSeq"]];
        NSString *accDes = [Utils getObjectOrNil:[dict objectForKey:@"accDes"]];
        af.name = [accSeq stringValue];
        af.value = accDes;
    }];
    
    return accDesList;
}

- (AutoSection *)addOptionSection:(NSString *)keyStr
{
    NSArray *optionList = [self.autoInfo objectForKey:keyStr];
    AutoSection *section = [[AutoSection alloc] init];
    [self.sectionList addObject:section];
    section.name = keyStr;
    section.afList = [self getAccDesList:optionList];
    return section;
}

- (AutoField *)getSubTotalField:(NSString *)keyStr title:(NSString *)titleStr
{
    NSString *priceStr = [Utils getStrOrNil:[self.autoInfo objectForKey:keyStr]];
    AutoField *af = [[AutoField alloc] init];
    af.name = titleStr;
    if (priceStr) {
        af.value = [NSString stringWithFormat:@"HK$%@", [priceStr thousandSeparatedString]];
    }
    else {
        af.value = [NSString stringWithFormat:@"HK$%@", [@"0" thousandSeparatedString]];
    }
    return af;
}

#pragma mark - UITableView

- (AutoField *)getFieldAtIndexPath:(NSIndexPath *)indexPath
{
    AutoSection *autoSection = [self.sectionList objectAtIndex:indexPath.section];
    return [autoSection.afList objectAtIndex:indexPath.row];
}

- (NSString *)description
{
    NSString *makeStr = [self.autoInfo objectForKey:@"makeDesc"];
    NSString *modelStr = [self.autoInfo objectForKey:@"modelName"];
    NSString *engineSize = [self.autoInfo objectForKey:@"engineSize"];
    NSString *manuYear = [self.autoInfo objectForKey:@"manufactYear"];
    return [NSString stringWithFormat:@"%@ %@ %@ %@", makeStr, modelStr, engineSize, manuYear];
}

@end
