//
//  RequestHeader.h
//  HK Car Tax
//
//  Created by Rocky Wong on 20/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestHeader : NSObject

@property (nonatomic) NSInteger version;

- (NSMutableDictionary *)getPostJSON;
- (NSMutableDictionary *)getHeaderJSON;

@end
