//
//  PRPTableViewCell.h
//  HK Car Tax
//
//  Created by Rocky Wong on 25/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRPTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *makeLbl;
@property (weak, nonatomic) IBOutlet UILabel *effDateLbl;
@property (strong, nonatomic) PRPQuery *query;

@end
