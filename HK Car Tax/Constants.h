//
//  Constants.h
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#ifndef HK_Car_Tax_Constants_h
#define HK_Car_Tax_Constants_h

#define SOLID_BLUE [UIColor colorWithRed:95/255.0 green:188/255.0 blue:205/255.0 alpha:1]
#define SOLID_LIGHT_BLUE [UIColor colorWithRed:175/255.0 green:221/255.0 blue:230/255.0 alpha:1]
#define SOLID_GREY [UIColor colorWithRed:96/255.0 green:96/255.0 blue:96/255.0 alpha:1]
#define TAB_GREEN [UIColor colorWithRed:189/255.0 green:219/255.0 blue:201/255.0 alpha:1]
#define THEME_GREEN [UIColor colorWithRed:0/255.0 green:127/255.0 blue:24/255.0 alpha:1]
#define LIGHT_GREEN [UIColor colorWithRed:0/255.0 green:127/255.0 blue:24/255.0 alpha:0.7]

#define ALERT_WILL_QUIT_TAG     992333
#define APP_STORE_ID            @"839981885"
// ********** Command for different enviroment **********//

//#define DEBUG 1
//#define DEVELOPMENT 1

#ifdef DEBUG
//// Staging Env
#define SERVER_HOST_NAME @"eservices.customs.gov.hk"
#define SERVER_URL [NSString stringWithFormat:@"http://%@", SERVER_HOST_NAME]
#define SECURE_SERVER_URL [NSString stringWithFormat:@"https://%@", SERVER_HOST_NAME]
#define GOOGLE_ANALYTICS_ID @""

#elif DEVELOPMENT
// Development Env
#define SERVER_HOST_NAME @"20.10.1.107:8080"
#define SERVER_URL [NSString stringWithFormat:@"http://%@", SERVER_HOST_NAME]
#define SECURE_SERVER_URL [NSString stringWithFormat:@"https://%@", SERVER_HOST_NAME]
#define GOOGLE_ANALYTICS_ID @""

#else
// Production Env
#define SERVER_HOST_NAME @"eservices.customs.gov.hk"
#define SERVER_URL [NSString stringWithFormat:@"http://%@", SERVER_HOST_NAME]
#define SECURE_SERVER_URL [NSString stringWithFormat:@"https://%@", SERVER_HOST_NAME]
#define GOOGLE_ANALYTICS_ID @""
//
#endif

// force update
#define FORCE_UPDATE_TAG @"f"

#define REQUEST_TIMEOUT 30

#define POST_APNS_TOKEN_URL [NSString stringWithFormat:@"%@/apns/:app_id/:user_id", SERVER_URL]
#define GET_ACCESS_TOKEN_URL [NSString stringWithFormat:@"%@/FRT-APP/webresources/frt/accessToken", SECURE_SERVER_URL]

// access token
#define APP_NAME @"C&ED Car Tax"
#define ACCESS_TOKEN_CHECK_VALUE @"z5FQVfrrK2RBujNwa9vUBuZr"
#define ACCESS_TOKEN_ENCRYPT_KEY @"64fGKTdkNajxanG4"

// check app upgrade
#define APP_CLIENT_TYPE @"iOS"
#define POST_TO_CHECK_UPGRADE_URL [NSString stringWithFormat:@"%@/FRT-APP/webresources/frt/checkAppUpgrade",  SECURE_SERVER_URL]

// post
#define POST_TO_GET_MAKE_LIST_URL [NSString stringWithFormat:@"%@/FRT-APP/webresources/frt/makeList",  SECURE_SERVER_URL]
#define POST_TO_GET_MODEL_LIST_URL [NSString stringWithFormat:@"%@/FRT-APP/webresources/frt/modelList", SECURE_SERVER_URL]
#define POST_SEARCH_URL [NSString stringWithFormat:@"%@/FRT-APP/webresources/frt/searchApprovedPrp", SECURE_SERVER_URL]
#define POST_TO_GET_ANNOUNCEMENT [NSString stringWithFormat:@"%@/FRT-APP/webresources/frt/textContent",  SECURE_SERVER_URL]

// URL
#define CALCULATOR_URL [NSString stringWithFormat:@"%@/FRT-APP/cal.jsp",  SECURE_SERVER_URL]
#define CALCULATOR_EN_URL [NSString stringWithFormat:@"%@/FRT-APP/cal_en.jsp",  SECURE_SERVER_URL]
#define CALCULATOR_TC_URL [NSString stringWithFormat:@"%@/FRT-APP/cal_tc.jsp",  SECURE_SERVER_URL]
#define CALCULATOR_SC_URL [NSString stringWithFormat:@"%@/FRT-APP/cal_sc.jsp",  SECURE_SERVER_URL]
#define CALCULATOR_INFO_URL [NSString stringWithFormat:@"%@/FRT-APP/calInfo.jsp", SECURE_SERVER_URL]
#define CALCULATOR_INFO_EN_URL [NSString stringWithFormat:@"%@/FRT-APP/calInfo_en.jsp", SECURE_SERVER_URL]
#define CALCULATOR_INFO_TC_URL [NSString stringWithFormat:@"%@/FRT-APP/calInfo_tc.jsp", SECURE_SERVER_URL]
#define CALCULATOR_INFO_SC_URL [NSString stringWithFormat:@"%@/FRT-APP/calInfo_sc.jsp", SECURE_SERVER_URL]
#define QUIZ_URL [NSString stringWithFormat:@"%@/FRT-APP/quiz.jsp",  SECURE_SERVER_URL]
#define NOTE_TO_BUYER_EN_URL [NSString stringWithFormat:@"%@/FRT-APP/notetobuyer_en.jsp",  SECURE_SERVER_URL]
#define NOTE_TO_BUYER_TC_URL [NSString stringWithFormat:@"%@/FRT-APP/notetobuyer_tc.jsp",  SECURE_SERVER_URL]
#define NOTE_TO_BUYER_SC_URL [NSString stringWithFormat:@"%@/FRT-APP/notetobuyer_sc.jsp",  SECURE_SERVER_URL]
#define NOTE_TO_BUYER_URL [NSString stringWithFormat:@"%@/FRT-APP/notetobuyer.jsp",  SECURE_SERVER_URL]
#define NOTE_TO_TRADER_URL [NSString stringWithFormat:@"%@/FRT-APP/notetotrader.jsp",  SECURE_SERVER_URL]
#define GLOSSARY_URL [NSString stringWithFormat:@"%@/FRT-APP/glossary.jsp",  SECURE_SERVER_URL]
#define LINK_TO_OTHERS_URL [NSString stringWithFormat:@"%@/FRT-APP/linkstoother.jsp",  SECURE_SERVER_URL]
#define VIDEO_URL [NSString stringWithFormat:@"%@/FRT-APP/clips/api_video.mp4",  SECURE_SERVER_URL]
#define SHARE_URL @"https://eservices.customs.gov.hk/FRT-APP/shareto.jsp"

// Expiry
#define VIDEO_EXPIRY_DATE @"2022-05-30"


#define shallAllowLandscape @"shallAllowLandscape"
#endif
