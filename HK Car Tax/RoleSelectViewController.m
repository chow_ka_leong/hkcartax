//
//  RoleSelectViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 27/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "RoleSelectViewController.h"
#import "AnnouncementViewController.h"
#import "DisclaimerViewController.h"
#import "MenuViewController.h"
#import "GDNavigationController.h"
#import "APIManager.h"

#define kOptionalAppUpgradeAlertBoxTag 10001
#define kForceAppUpgradeAlertBoxTag 10002

@interface RoleSelectViewController () <UIAlertViewDelegate>

@property (nonatomic, strong) NSString *upgradeUrl;

@end

@implementation RoleSelectViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        Language *lang = [Language sharedInstance];
        self.navigationItem.title = [lang get:@"appName"];
        self.navigationItem.hidesBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"green_gradient.png"]];
    
    Language *lang = [Language sharedInstance];
    self.titleLbl.text = [lang get:@"roleSelectTitle"];
    self.versionLbl.text = [Utils appVersion];

    NSString *buyerStr = @"userpage_btn_buyer";
    NSString *traderStr = @"userpage_btn_trader";
    if ([lang tc]) {
        buyerStr = [buyerStr stringByAppendingString:@"_tc.png"];
        traderStr = [traderStr stringByAppendingString:@"_tc.png"];
    } else if ([lang sc]) {
        buyerStr = [buyerStr stringByAppendingString:@"_sc.png"];
        traderStr = [traderStr stringByAppendingString:@"_sc.png"];
    } else {
        buyerStr = [buyerStr stringByAppendingString:@"_en.png"];
        traderStr = [traderStr stringByAppendingString:@"_en.png"];
    }
    
    [self.buyerBtn setImage:[UIImage imageNamed:buyerStr] forState:UIControlStateNormal];
    [self.traderBtn setImage:[UIImage imageNamed:traderStr] forState:UIControlStateNormal];
    
    self.buyerBtn.accessibilityLabel = [lang get:@"buyer"];
    self.traderBtn.accessibilityLabel = [lang get:@"trader"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self checkAppUpgrade];
//    [self openDisclaimer];
}

-(void)openDisclaimer {
    
    RoleManager *roleMgr = [RoleManager sharedManager];
    if (![roleMgr isDisclaimerAccepted]) {
        DisclaimerViewController *disclaimerCtrl = [Utils getStoryboardViewController:@"DisclaimerViewController"];
        GDNavigationController *navCtrl = [[GDNavigationController alloc] initWithRootViewController:disclaimerCtrl];
        [self presentViewController:navCtrl animated:NO completion:nil];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
*/

#pragma mark - Events

- (IBAction)rolePressed:(UIButton *)sender
{
    if (![Utils isServerReachable]) {
        [Utils showNotReachable:self];
        return;
    }
    
    RoleManager *roleMgr = [RoleManager sharedManager];
    if (sender == self.traderBtn)
        roleMgr.isTrader = YES;
    else
        roleMgr.isTrader = NO;
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [roleMgr getAnnouncement:^(NSString *contentStr, BOOL maintenance, BOOL invalidTime, BOOL timeout) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
        // maintenance
        if (timeout) {
            [Utils showOnMaintenance:self serverDown:YES];
            
            // invalid handset time
        } else if (maintenance) {
            [Utils showOnMaintenance:self serverDown:NO];
            
            // invalid handset time
        } else if (invalidTime) {
            Language *lang = [Language sharedInstance];
            [Utils showAlertWithTitle:[lang get:@"errorInvalidHandsetTime"] message:nil];
        
            // has announcement
        } else if ([contentStr length] > 0) {
            AnnouncementViewController *announcementCtrl = [Utils getStoryboardViewController:@"AnnouncementViewController"];
            announcementCtrl.contentStr = contentStr;
            [self.navigationController pushViewController:announcementCtrl animated:YES];
        
            // no announcement
        } else  if ([contentStr isEqualToString:@""]) {
            MenuViewController *menuCtrl = [Utils getStoryboardViewController:@"MenuViewController"];
            [self.navigationController pushViewController:menuCtrl animated:YES];
            
        } else {
            Language *lang = [Language sharedInstance];
            [Utils showAlertWithTitle:[lang get:@"errorNetworkNotReachable"] message:nil];
        }
    }];
}

//-(void) checkAppUpgrade {
//    
//    if (![Utils isServerReachable]) {
//        [Utils showNotReachable];
//        return;
//    }
//    
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    params[@"appClient"] = APP_CLIENT_TYPE;
//    params[@"appVer"] = [Utils appVersion];
//    
//    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    
//    APIManager *apiMgr = [APIManager sharedManager];
//    [apiMgr postCheckAppUpgrade:params callback:^(NSData *json, NSHTTPURLResponse *response) {
//        
//        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
//        
//        NSDictionary *resultDict = nil;
//        BOOL forceUpgrade = NO;
//        NSString *upgradeUrl = @"";
//        
//        if (json) {
//            resultDict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
//            forceUpgrade = [[resultDict objectForKey:@"forceUpgrade"] boolValue];
//            upgradeUrl = [Utils getStrOrNil:[resultDict objectForKey:@"upgradeUrl"]];
//        }
//        
//        self.upgradeUrl = upgradeUrl;
//        
//        // timeout or
//        // maintenance
//        if ([Utils onServerTimeout:response] || [Utils onMaintenance:response]) {
////            [Utils showOnMaintenance:self];
//            
//        // invalid handset time
//        } else if (apiMgr.invalidHandsetTime) {
//            Language *lang = [Language sharedInstance];
//            [Utils showAlertWithTitle:[lang get:@"errorInvalidHandsetTime"] message:nil];
//            
//        } else if (upgradeUrl != nil && ![upgradeUrl isEqualToString:@""]) {
//            
//            Language *lang = [Language sharedInstance];
//            
//            NSString *title = [lang get:forceUpgrade ? @"appUpdateForceMessage":@"appUpdateMessage"];
//            
//            if (NSStringFromClass([UIAlertController class])) {
//                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
//                
//                [alertController addAction:[UIAlertAction actionWithTitle:[lang get:@"update"]
//                                                                    style:UIAlertActionStyleDefault
//                                                                  handler:^(UIAlertAction *action){
//                                                                      [self goToAppStore];
//                                                                  }]];
//                
//                
//                [alertController addAction:[UIAlertAction actionWithTitle:[lang get:@"cancel"]
//                                                                    style:UIAlertActionStyleDefault
//                                                                  handler:^(UIAlertAction *action){
//                                                                      if (!forceUpgrade) {
//                                                                          [self openDisclaimer];
//                                                                      } else {
//                                                                          exit(0);
//                                                                      }
//                                                                  }]];
//                
//                
//                alertController.view.frame = [[UIScreen mainScreen] applicationFrame];
//                [self presentViewController:alertController animated:YES completion:nil];
//            } else {
//                
//                UIAlertView *alert;
//                
//                alert = [[UIAlertView alloc] initWithTitle:title
//                                                   message:nil
//                                                  delegate:self
//                                         cancelButtonTitle:[lang get:@"cancel"]
//                                         otherButtonTitles:[lang get:@"update"], nil];
//                
//                if (forceUpgrade) {
//                    alert.tag = kForceAppUpgradeAlertBoxTag;
//                } else {
//                    alert.tag = kOptionalAppUpgradeAlertBoxTag;
//                }
//                
//                [alert show];
//            }
//            
//            if (forceUpgrade) {
//                self.traderBtn.hidden = YES;
//                self.buyerBtn.hidden = YES;
//                self.titleLbl.hidden = YES;
//            }
//        } else {
//            Language *lang = [Language sharedInstance];
//            [Utils showAlertWithTitle:[lang get:@"errorNetworkNotReachable"] message:nil];
//        }
//    }];
//}
//
//-(void)goToAppStore {
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.upgradeUrl]];
//}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
    
//    if (alertView.tag == kForceAppUpgradeAlertBoxTag) {
//        if (buttonIndex == 1) {
//            [self goToAppStore];
//        } else {
//            exit(0);
//        }
//    } else if (alertView.tag == kOptionalAppUpgradeAlertBoxTag) {
//        
//        if (buttonIndex == 1) {
//            [self goToAppStore];
//        } else {
//            [self openDisclaimer];
//        }
//    }
    
}

@end
