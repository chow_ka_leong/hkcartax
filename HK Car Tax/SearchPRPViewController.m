//
//  SearchPRPViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 17/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "SearchPRPViewController.h"
#import "AutoInfoViewController.h"
#import "SearchManager.h"
#import "ActionSheetDatePicker.h"
#import "ActionSheetStringPicker.h"

@interface SearchPRPViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) UITextField *activeTextField;
@property (nonatomic) float oY;
@property (nonatomic) float oHeight;
@property (strong, nonatomic) Make *make;
@property (copy, nonatomic) NSDate *effDate;

@property (strong, nonatomic) UIPickerView *makePickerView;
@property (strong, nonatomic) UIDatePicker *datePickerView;

@property (strong, nonatomic) UIToolbar *makeToolBar;
@property (strong, nonatomic) UIToolbar *dateToolBar;

- (void)initializePickerView;

@end

@implementation SearchPRPViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped)];
    tapRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapRecognizer];
    
    Language *lang = [Language sharedInstance];
    self.titleLbl.text = [lang get:@"searchTitle3"];
    self.makeTextField.placeholder = [lang get:@"searchHintMake"];
    
    self.makeTextField.delegate = self;
    self.makeTextField.autoCompleteDataSource = self;
    self.makeTextField.autoCompleteDelegate = self;
    self.makeTextField.autoCompleteTableCellBackgroundColor = [UIColor whiteColor];
    self.makeTextField.showAutoCompleteTableWhenEditingBegins = YES;
    
    self.prpLbl.text = [lang get:@"searchLabelPRP"];
    self.prpLbl.adjustsFontSizeToFitWidth = YES;
    self.effDateTextField.placeholder = [lang get:@"searchHintEffDate"];
    self.mandatoryLbl.text = [lang get:@"remarkMandatoryPlural"];
    [self.searchBtn setTitle:[lang get:@"search"] forState:UIControlStateNormal];
    [self.resetBtn setTitle:[lang get:@"reset"] forState:UIControlStateNormal];
    
    UIImage *blueImage = [[UIImage imageNamed:@"btn_blue.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    [self.searchBtn setBackgroundImage:blueImage forState:UIControlStateNormal];
    
    UIImage *greyImage = [[UIImage imageNamed:@"btn_grey.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    [self.resetBtn setBackgroundImage:greyImage forState:UIControlStateNormal];
    
    [self initializePickerView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.oY = self.view.frame.origin.y;
    self.oHeight = self.view.frame.size.height;
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Events

- (IBAction)searchPressed
{
    [self.activeTextField resignFirstResponder];
    NSString *errorStr = @"";
    Language *lang = [Language sharedInstance];
    
    if (self.make == nil)
        errorStr = [errorStr stringByAppendingFormat:@"%@\n", [lang get:@"errorMakeRequired"]];
    
    if ([self.prpTextField.text length] == 0)
        errorStr = [errorStr stringByAppendingFormat:@"%@\n", [lang get:@"errorPRPRequired"]];
    
    if ([self.prpYearTextField.text length] == 0)
        errorStr = [errorStr stringByAppendingFormat:@"%@\n", [lang get:@"errorPRPYearRequired"]];
    else if ([self.prpYearTextField.text length] != 2)
        errorStr = [errorStr stringByAppendingFormat:@"%@\n", [lang get:@"errorInvalidPRPYear"]];
    
    if (self.effDate == nil)
        errorStr = [errorStr stringByAppendingFormat:@"%@\n", [lang get:@"errorEffDateRequired"]];
    
    if ([errorStr length] > 0) {
        errorStr = [errorStr substringToIndex:[errorStr length] - 1];
        [Utils showAlertWithTitle:errorStr message:nil];
        
    } else {
        PRPQuery *query = [[PRPQuery alloc] init];
        query.make = self.make;
        query.prpStr = self.prpTextField.text;
        query.prpYearStr = self.prpYearTextField.text;
        query.effDate = self.effDate;

        SearchManager *searchMgr = [SearchManager sharedManager];
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [searchMgr searchByPRP:query saveHistory:YES callback:^(NSArray *resultList, NSString *errorStr) {
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            
            if (errorStr) {
                if ([errorStr isEqualToString:[lang get:@"errorServerNotAvailable"]]) {
                    [Utils showOnMaintenance:self serverDown:YES];
                } else if ([errorStr isEqualToString:[lang get:@"errorServerMaintenance"]]) {
                    [Utils showOnMaintenance:self serverDown:NO];
                } else {
                    [Utils showAlertWithTitle:errorStr message:nil];
                }
            } else if (resultList == nil) {
                [Utils showAlertWithTitle:[lang get:@"noPRPRecord"] message:nil];
            } else {
                AutoDetail *autoDetail = [resultList objectAtIndex:0];
                AutoInfoViewController *autoViewCtrl = [Utils getStoryboardViewController:@"AutoInfoViewController"];
                autoViewCtrl.autoDetail = autoDetail;
                [self.navigationController pushViewController:autoViewCtrl animated:YES];
            }
        }];
    }
}

- (IBAction)resetPressed
{
    self.makeTextField.text = nil;
    self.prpTextField.text = nil;
    self.prpYearTextField.text = nil;
    self.effDateTextField.text = nil;
    self.make = nil;
    self.effDate = nil;
}


- (IBAction)viewTapped
{
    [self.activeTextField resignFirstResponder];
}

- (void)pickerDoneClicked
{
    SearchManager *searchMgr = [SearchManager sharedManager];
    
    if (self.activeTextField == self.makeTextField) {
        NSUInteger selectedRow = [self.makePickerView selectedRowInComponent:0];
        NSArray *itemList = [searchMgr getMakeStrList];
        if (![[itemList objectAtIndex:selectedRow] isEqualToString:self.makeTextField.text]) {
            self.makeTextField.text = [itemList objectAtIndex:selectedRow];
            self.make = [searchMgr getMakeWithName:[itemList objectAtIndex:selectedRow]];
        }
    }
    else
    {
        self.effDate = self.datePickerView.date;
        NSString *dateFormat = @"MMM dd, yyyy";
        if ([[Language sharedInstance]tc] || [[Language sharedInstance]sc]) {
            dateFormat = [NSString stringWithFormat:@"yyyy年M月dd日"];
        }
        self.effDateTextField.text = [Utils dateStringWithDateFormat:dateFormat date:self.datePickerView.date];
    }
    
    [self.activeTextField resignFirstResponder];
}

- (void)pickerCancelClicked
{
    [self.activeTextField resignFirstResponder];
}


#pragma mark - Initialization

- (void)initializePickerView
{
    Language *lang = [Language sharedInstance];
    
//    self.makePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43 , 320, (self.view.height/2)-63.0)];
//    self.makePickerView.dataSource = self;
//    self.makePickerView.delegate = self;
//    [self.makePickerView  setShowsSelectionIndicator:YES];
    
    self.datePickerView = [[UIDatePicker alloc] init];
    self.datePickerView.datePickerMode = UIDatePickerModeDate;
    
    // Create done button in UIPickerView
//    self.makeToolBar = [self createToolBarWithTitle:[lang get:@"makeDesc"]];
    self.dateToolBar = [self createToolBarWithTitle:[lang get:@"effDate"]];
    
//    self.makeTextField.inputView = self.makePickerView;
//    self.makeTextField.inputAccessoryView = self.makeToolBar;
    
    self.effDateTextField.inputAccessoryView = self.dateToolBar;
    self.effDateTextField.inputView = self.datePickerView;
    
}

- (UIToolbar *)createToolBarWithTitle:(NSString *)title
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 56)];
    toolBar.barStyle = UIBarStyleDefault;
    //[toolBar sizeToFit];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pickerCancelClicked)];
    
    UIBarButtonItem *flexSpace1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *titleBtn = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:nil action:nil];
    titleBtn.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *flexSpace2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked)];
    
    NSArray *barItems = [NSArray arrayWithObjects: cancelBtn, flexSpace1, titleBtn, flexSpace2, doneBtn, nil];
    
    [toolBar setItems:barItems animated:YES];
    
    return toolBar;
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.activeTextField resignFirstResponder];
    self.activeTextField = textField;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Keyboard

- (void)keyboardWillShow {
    
    if ([self.activeTextField isEqual:self.makeTextField]) {
        return;
    }
    
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= self.oY)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < self.oY)
    {
        [self setViewMovedUp:NO];
    }
}

- (void)keyboardWillHide {
    
    if ([self.activeTextField isEqual:self.makeTextField]) {
        return;
    }
    
    if (self.view.frame.origin.y >= self.oY)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < self.oY)
    {
        [self setViewMovedUp:NO];
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
- (void)setViewMovedUp:(BOOL)movedUp
{
    float keyboardOffset = [Utils isIPhone5] ? 40 : 70;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y = -keyboardOffset;
        rect.size.height = self.oHeight + keyboardOffset;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y = 0;
        rect.size.height = self.oHeight;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    SearchManager *searchMgr = [SearchManager sharedManager];
    return [[searchMgr getMakeStrList]count];
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component

{
    SearchManager *searchMgr = [SearchManager sharedManager];
    return [[searchMgr getMakeStrList] objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {}


#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isKindOfClass:[UITableViewCell class]] ) {
        return NO;
    }
    
    if([touch.view.superview isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    
    return YES;
}

#pragma mark - MLPAutoCompleteTextField DataSource


//example of asynchronous fetch:
- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
            completionHandler:(void (^)(NSArray *))handler
{
    SearchManager *searchMgr = [SearchManager sharedManager];
    handler([searchMgr getMakeStrList]);
}


#pragma mark - MLPAutoCompleteTextField Delegate

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selected = nil;
    if(selectedObject){
        //        NSLog(@"selected object from autocomplete menu %@ with string %@", selectedObject, [selectedObject autocompleteString]);
        selected = [selectedObject autocompleteString];
    } else {
        //        NSLog(@"selected string '%@' from autocomplete menu", selectedString);
        selected = selectedString;
    }
    
    SearchManager *searchMgr = [SearchManager sharedManager];
    
    self.makeTextField.text = selected;
    self.make = [searchMgr getMakeWithName:selected];
    
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    //    NSLog(@"Autocomplete table view will be removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    //    NSLog(@"Autocomplete table view will be added to the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    //    NSLog(@"Autocomplete table view ws removed from the view hierarchy");
    SearchManager *searchMgr = [SearchManager sharedManager];
    if (textField == self.makeTextField) {
        self.make = [searchMgr getMakeWithName:textField.text];
        
        if (self.make == nil) {
            self.makeTextField.text = nil;
            self.make = nil;
        }
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    //    NSLog(@"Autocomplete table view was added to the view hierarchy");
}

@end
