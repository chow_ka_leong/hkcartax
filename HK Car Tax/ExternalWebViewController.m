//
//  ExternalWebViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 27/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "ExternalWebViewController.h"

@interface ExternalWebViewController ()

@end

@implementation ExternalWebViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (![request.URL isEqual:self.request.URL]) {
        
        if ([request.URL.absoluteString isEqual:@"about:blank"]) {
            return YES;
        }
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
        
    } else {
        return YES;
    }
}

@end
