//
//  SearchHistoryViewController.m
//  HK Car Tax
//
//  Created by Rocky Wong on 24/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import "SearchHistoryViewController.h"
#import "SearchResultListViewController.h"
#import "AutoInfoViewController.h"
#import "ModelTableViewCell.h"
#import "ChassisTableViewCell.h"
#import "PRPTableViewCell.h"
#import "HistoryManager.h"
#import "SearchManager.h"

@interface SearchHistoryViewController ()

@end

@implementation SearchHistoryViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.historyList count] == 0) {
        UILabel *noRecordLbl = [Utils getNoContentLabel];
        noRecordLbl.center = self.view.center;
        self.tableView.hidden = YES;
        [self.view addSubview:noRecordLbl];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.historyList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ModelCellIdentifier = @"ModelCellIdentifier";
    static NSString *ChassisCellIdentifier = @"ChassisCellIdentifier";
    static NSString *PRPCellIdentifier = @"PRPCellIdentifier";
    
    SearchQuery *query = [self.historyList objectAtIndex:indexPath.row];
    
    UITableViewCell *cell;
    
    if ([query isKindOfClass:[ModelQuery class]]) {
        cell = [tableView dequeueReusableCellWithIdentifier:ModelCellIdentifier forIndexPath:indexPath];
        ((ModelTableViewCell *)cell).query = (ModelQuery *)query;
        
    } else if ([query isKindOfClass:[ChassisQuery class]]) {
        cell = [tableView dequeueReusableCellWithIdentifier:ChassisCellIdentifier forIndexPath:indexPath];
        ((ChassisTableViewCell *)cell).query = (ChassisQuery *)query;
        
    } else if ([query isKindOfClass:[PRPQuery class]]) {
        cell = [tableView dequeueReusableCellWithIdentifier:PRPCellIdentifier forIndexPath:indexPath];
        ((PRPTableViewCell *)cell).query = (PRPQuery *)query;
    }
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchQuery *query = [self.historyList objectAtIndex:indexPath.row];
    
    if ([query isKindOfClass:[ModelQuery class]]) {
        return 105;
        
    } else if ([query isKindOfClass:[ChassisQuery class]]) {
        return 45;
        
    } else if ([query isKindOfClass:[PRPQuery class]]) {
        return 85;
    }
    
    return 105;
}

@end

#pragma mark - Subclasses

@implementation HistoryVehicleViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        HistoryManager *historyMgr = [HistoryManager sharedManager];
        self.historyList = historyMgr.modelList;
    }
    return self;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SearchManager *searchMgr = [SearchManager sharedManager];
    SearchQuery *query = [self.historyList objectAtIndex:indexPath.row];
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [searchMgr search:query saveHistory:NO callback:^(NSArray *resultList, NSString *errorStr) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
        if (errorStr) {
            Language *lang = [Language sharedInstance];
            if ([errorStr isEqualToString:[lang get:@"errorServerNotAvailable"]]) {
                [Utils showOnMaintenance:self serverDown:YES];
            } else if ([errorStr isEqualToString:[lang get:@"errorServerMaintenance"]]) {
                [Utils showOnMaintenance:self serverDown:NO];
            } else {
                [Utils showAlertWithTitle:errorStr message:nil];
            }
            
        } else if (resultList == nil) {
            Language *lang = [Language sharedInstance];
            [Utils showAlertWithTitle:[lang get:@"noModelRecord"] message:nil];
            
        } else if ([resultList count] > 1) {
            SearchResultListViewController *resultViewCtrl = [Utils getStoryboardViewController:@"SearchResultListViewController"];
            resultViewCtrl.resultList = resultList;
            [self.navigationController pushViewController:resultViewCtrl animated:YES];
            
        } else {
            AutoDetail *autoDetail = [resultList objectAtIndex:0];
            AutoInfoViewController *autoViewCtrl = [Utils getStoryboardViewController:@"AutoInfoViewController"];
            autoViewCtrl.autoDetail = autoDetail;
            [self.navigationController pushViewController:autoViewCtrl animated:YES];
        }
    }];
}


#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
}

@end

@implementation HistoryChassisViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        HistoryManager *historyMgr = [HistoryManager sharedManager];
        self.historyList = historyMgr.chassisList;
    }
    return self;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SearchManager *searchMgr = [SearchManager sharedManager];
    SearchQuery *query = [self.historyList objectAtIndex:indexPath.row];
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [searchMgr search:query saveHistory:NO callback:^(NSArray *resultList, NSString *errorStr) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
        if (errorStr) {
            Language *lang = [Language sharedInstance];
            if ([errorStr isEqualToString:[lang get:@"errorServerNotAvailable"]]) {
                [Utils showOnMaintenance:self serverDown:YES];
            } else if ([errorStr isEqualToString:[lang get:@"errorServerMaintenance"]]) {
                [Utils showOnMaintenance:self serverDown:NO];
            } else {
                [Utils showAlertWithTitle:errorStr message:nil];
            }
        } else if (resultList == nil) {
            Language *lang = [Language sharedInstance];
            [Utils showAlertWithTitle:[lang get:@"noChassisRecord"] message:nil];
        } else {
            AutoDetail *autoDetail = [resultList objectAtIndex:0];
            AutoInfoViewController *autoViewCtrl = [Utils getStoryboardViewController:@"AutoInfoViewController"];
            autoViewCtrl.autoDetail = autoDetail;
            [self.navigationController pushViewController:autoViewCtrl animated:YES];
        }
    }];
}


#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
}

@end

@implementation HistoryPRPViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        HistoryManager *historyMgr = [HistoryManager sharedManager];
        self.historyList = historyMgr.prpList;
    }
    return self;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SearchManager *searchMgr = [SearchManager sharedManager];
    SearchQuery *query = [self.historyList objectAtIndex:indexPath.row];
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [searchMgr search:query saveHistory:NO callback:^(NSArray *resultList, NSString *errorStr) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
        if (errorStr) {
            Language *lang = [Language sharedInstance];
            if ([errorStr isEqualToString:[lang get:@"errorServerNotAvailable"]]) {
                [Utils showOnMaintenance:self serverDown:YES];
            } else if ([errorStr isEqualToString:[lang get:@"errorServerMaintenance"]]) {
                [Utils showOnMaintenance:self serverDown:NO];
            } else {
                [Utils showAlertWithTitle:errorStr message:nil];
            }
        } else if (resultList == nil) {
            Language *lang = [Language sharedInstance];
            [Utils showAlertWithTitle:[lang get:@"noPRPRecord"] message:nil];
        } else {
            AutoDetail *autoDetail = [resultList objectAtIndex:0];
            AutoInfoViewController *autoViewCtrl = [Utils getStoryboardViewController:@"AutoInfoViewController"];
            autoViewCtrl.autoDetail = autoDetail;
            [self.navigationController pushViewController:autoViewCtrl animated:YES];
        }
    }];
}


#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERT_WILL_QUIT_TAG) {
        exit(0);
    }
}

@end
