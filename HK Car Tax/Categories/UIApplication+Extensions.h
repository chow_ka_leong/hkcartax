//
//  UIApplication+Extensions.h
//  HK Car Tax
//
//  Created by Sheon Lee on 3/13/15.
//  Copyright (c) 2015 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (Extensions)

- (UIViewController *)topViewController;
- (UIViewController *)topViewController:(UIViewController *)rootViewController;

@end
