//
//  NSString+GDExt.h
//  Kitec
//
//  Created by Rocky Wong on 8/1/14.
//  Copyright (c) 2014 Golden Dynamic Enterprise Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GDExt)

- (NSString *)stringbyRemovingHTMLSpaces;
- (BOOL)isDigitsOnly;
- (NSString *)trimString;
- (NSString *)reverseString;
- (NSString *)stringValue;
- (NSString *)thousandSeparatedString;

#pragma mark - Credit Card

- (BOOL)isAmericanExpress;
- (BOOL)isValidCreditCard;
- (NSString *)creditCardType;

@end
