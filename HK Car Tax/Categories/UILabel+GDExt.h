//
//  UILabel+GDExt.h
//  Kitec
//
//  Created by Rocky Wong on 31/12/13.
//  Copyright (c) 2013 Golden Dynamic Enterprise Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (GDExt)

- (void)setMultipleLineTextAndResize:(NSString *)text;

@end
