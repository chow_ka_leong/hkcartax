//
//  UILabel+GDExt.m
//  Kitec
//
//  Created by Rocky Wong on 31/12/13.
//  Copyright (c) 2013 Golden Dynamic Enterprise Limited. All rights reserved.
//

#import "UILabel+GDExt.h"

@implementation UILabel (GDExt)

- (void)setMultipleLineTextAndResize:(NSString *)text
{
    CGSize size = [text sizeWithFont:self.font constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, ceilf(size.height));
    self.lineBreakMode = NSLineBreakByWordWrapping;
    self.numberOfLines = 0;
    self.text = text;
}

@end
