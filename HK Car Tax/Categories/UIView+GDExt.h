//
//  UIView+GDExt.h
//  Kitec
//
//  Created by Rocky Wong on 31/12/13.
//  Copyright (c) 2013 Golden Dynamic Enterprise Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (GDExt)

- (void)setOrigin:(CGPoint)origin;
- (void)setSize:(CGSize)size;
- (void)setWidth:(float)width;
- (void)setHeight:(float)height;
- (void)setX:(float)x;
- (void)setY:(float)y;
- (void)setCenterX:(float)centerX;
- (void)setCenterY:(float)centerY;

- (float)x;
- (float)y;
- (float)width;
- (float)height;
- (float)bottom;

@end
