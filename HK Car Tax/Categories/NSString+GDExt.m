//
//  NSString+GDExt.m
//  Kitec
//
//  Created by Rocky Wong on 8/1/14.
//  Copyright (c) 2014 Golden Dynamic Enterprise Limited. All rights reserved.
//

#import "NSString+GDExt.h"

@implementation NSString (GDExt)

- (NSString *)stringbyRemovingHTMLSpaces
{
    NSString *resultStr = [self stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:nil];
    resultStr = [regex stringByReplacingMatchesInString:resultStr
                                                options:0
                                                  range:NSMakeRange(0, [resultStr length])
                                           withTemplate:@" "];
    return resultStr;
}

- (BOOL)isDigitsOnly
{
    NSCharacterSet *numericSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *currentSet = [NSCharacterSet characterSetWithCharactersInString:self];
    return [numericSet isSupersetOfSet:currentSet];
}

- (NSString *)trimString
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)reverseString
{
    NSMutableString *reversedStr = [NSMutableString stringWithCapacity:[self length]];
    for (NSInteger i = [self length] - 1; i >= 0; i--)
        [reversedStr appendFormat:@"%c", [self characterAtIndex:i]];
    
    return [NSString stringWithString:reversedStr];
}

- (NSString *)stringValue
{
    return [self copy];
}

- (NSString *)thousandSeparatedString
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *num = [formatter numberFromString:self];
    formatter.usesGroupingSeparator = YES;
    formatter.groupingSize = 3;
//    formatter.minimumFractionDigits = 2;
    return [formatter stringFromNumber:num];
}

#pragma mark - Credit Card

- (BOOL)isAmericanExpress
{
    if ([self rangeOfString:@"34"].location == 0)
        return YES;
    
    if ([self rangeOfString:@"37"].location == 0)
        return YES;
    
    return NO;
}

- (BOOL)isValidCreditCard
{
    NSUInteger length = [self length];
    
    if (![self isDigitsOnly] || length < 13 || length > 16) {
        return NO;
    }
    
    // VISA
    int firstDigit = [[self substringWithRange:NSMakeRange(0, 1)] intValue];
    if (firstDigit == 4) {
        if (length != 13 && length != 16)
            return NO;
    }
    
    int firstTwoDigits = [[self substringWithRange:NSMakeRange(0, 2)] intValue];
    
    // MASTER
    if (firstTwoDigits >= 50 && firstTwoDigits <= 55) {
        if (length != 16)
            return NO;
    }
    
    // AMEX
    if (firstTwoDigits == 34 || firstTwoDigits == 37) {
        if (length != 15)
            return NO;
    }
    
    // Luhn Formula
    NSString *lastDigitRemoved = [self substringToIndex:[self length] - 1];
    NSString *reversed = [lastDigitRemoved reverseString];
    
    int sum = 0;
    for (int i = 0; i < [reversed length]; i++) {
        // odd digit
        if (i % 2 == 0) {
            int digit = [reversed characterAtIndex:i] - 48;
            int timesTwo = digit * 2;
            if (timesTwo < 10)
                sum += timesTwo;
            else
                sum += timesTwo - 9;
            
        // even digit
        } else {
            int digit = [reversed characterAtIndex:i] - 48;
            sum += digit;
        }
    }
    
    int lastDigit = [self characterAtIndex:[self length] - 1] - 48;
    if ((sum + lastDigit) % 10 == 0)
        return YES;
    else
        return NO;
}

- (NSString *)creditCardType
{
    if (![self isValidCreditCard])
        return nil;
    
    // VISA
    int firstDigit = [[self substringWithRange:NSMakeRange(0, 1)] intValue];
    if (firstDigit == 4) {
        return @"VISA";
    }
    
    int firstTwoDigits = [[self substringWithRange:NSMakeRange(0, 2)] intValue];
    
    // MASTER
    if (firstTwoDigits >= 50 && firstTwoDigits <= 55) {
        return @"Master";
    }
    
    // AMEX
    if (firstTwoDigits == 34 || firstTwoDigits == 37) {
        return @"AMEX";
    }
    
    return nil;
}

@end
