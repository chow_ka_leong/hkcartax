//
//  UIView+GDExt.m
//  Kitec
//
//  Created by Rocky Wong on 31/12/13.
//  Copyright (c) 2013 Golden Dynamic Enterprise Limited. All rights reserved.
//

#import "UIView+GDExt.h"

@implementation UIView (GDExt)

- (void)setOrigin:(CGPoint)origin
{
    CGRect rect = self.frame;
    rect.origin = origin;
    self.frame = rect;
}
- (void)setSize:(CGSize)size
{
    CGRect rect = self.frame;
    rect.size = size;
    self.frame = rect;
}
- (void)setWidth:(float)width
{
    CGRect rect = self.frame;
    rect.size.width = width;
    self.frame = rect;
}
- (void)setHeight:(float)height
{
    CGRect rect = self.frame;
    rect.size.height = height;
    self.frame = rect;
}
- (void)setX:(float)x
{
    CGRect rect = self.frame;
    rect.origin.x = x;
    self.frame = rect;
}
- (void)setY:(float)y
{
    CGRect rect = self.frame;
    rect.origin.y = y;
    self.frame = rect;
}
- (void)setCenterX:(float)centerX
{
    CGPoint point = self.center;
    point.x = centerX;
    self.center = point;
}
- (void)setCenterY:(float)centerY
{
    CGPoint point = self.center;
    point.y = centerY;
    self.center = point;
}
- (float)x
{
    return self.frame.origin.x;
}
- (float)y
{
    return self.frame.origin.y;
}
- (float)width
{
    return self.frame.size.width;
}
- (float)height
{
    return self.frame.size.height;
}
- (float)bottom
{
    return self.frame.origin.y + self.frame.size.height;
}

@end
