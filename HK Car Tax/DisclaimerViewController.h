//
//  DisclaimerViewController.h
//  HK Car Tax
//
//  Created by Rocky Wong on 28/3/14.
//  Copyright (c) 2014 Rocky Wong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisclaimerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *declineBtn;

- (IBAction)acceptPressed;
- (IBAction)declinePressed;

@end
